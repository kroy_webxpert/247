<!DOCTYPE html>
<html lang="en">
    <head>
        <link href="https://plus.google.com/110412304848758494023" rel="publisher">
        <meta name="google-site-verification" content="0gfTllPwir_ODw8WbpLfTlCxYKeAOlwjPM34Z0TEmmQ" />
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="GDS">
        <meta name="Copyright" content="Copyright 2015 247VirtualAssistant">
        <meta name="Content-Language" content="en-us">
        <meta name="description" content="Virtual assistant Testimonials">
        <meta name="keywords" content="Virtual Assistant,Personal Assistant,virtual personal assistant,your virtual assistant,personal assistant virtual">
        <title>Testimonials | 24/7 Virtual Assistant</title>
                <!-- Bootstrap -->
        <link href="https://247virtualassistants.com/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <!--<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">-->

        <link href="css/styles.css" rel="stylesheet">
        <link href="https://247virtualassistants.com/css/styles_dev.css" rel="stylesheet">
        <link href="https://247virtualassistants.com/css/mediaqueries.css" rel="stylesheet">
        <link href="https://247virtualassistants.com/fonts/stylesheet.css" rel="stylesheet">
        <link href='//fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,200,600' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,200,600,700,900' rel='stylesheet' type='text/css'>
        <!--affiliate-->
        <link type="text/css" rel="stylesheet" href="https://247virtualassistants.com/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
        <script type="text/javascript" src="https://247virtualassistants.com/dhtmlgoodies_calendar1.js?random=20060118"></script>
        <script language="javascript" src="https://247virtualassistants.com/js/validates.js"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="https://247virtualassistants.com/js/bootstrap.min.js"></script>
        <script src="https://247virtualassistants.com/js/responsive-tabs.js"></script>
<!--        <script src="lib/jquery.js"></script>-->
        <script src="https://247virtualassistants.com/dist/jquery.validate.js"></script>
      <!--  <script src="https://247virtualassistants.com/captcha.js"></script>-->
                <!-- Add jQuery library -->
<script type="text/javascript" src="https://code.jquery.com/jquery-latest.min.js"></script>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="https://247virtualassistants.com/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

<!-- Add fancyBox -->
<link rel="stylesheet" href="https://247virtualassistants.com/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="https://247virtualassistants.com/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>

<!-- Optionally add helpers - button, thumbnail and/or media -->
<link rel="stylesheet" href="https://247virtualassistants.com/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="https://247virtualassistants.com/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="https://247virtualassistants.com/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

<link rel="stylesheet" href="https://247virtualassistants.com/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="https://247virtualassistants.com/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
<script src="https://www.youtube.com/player_api"></script>
<script type="text/javascript" src="https://247virtualassistants.com/js/script.js"></script>
<!--<script type="text/javascript" src="http://192.168.1.50/virtualassistant/247virtualassistant/js/script.js"></script>-->    </head>
<body>
 

<div class="top">
<div class="banner_device"><img src="images/banner_subdevice.png" alt="virtual-assistance-testimonials" /></div>
  <div class="banner_sub">
  <div class="container">
  <div class="col-lg-12">
  <div class="bannertext ">
    
  <span><img src="images/leftborder.png" alt="leftborder"/></span> <h3>Testimonials</h3><span><img src="images/rightborder.png" alt="rightborder"/></span>
  </div>
  </div>
  
  </div>
  </div>
</div>
  
  <div class="container-top">
  <div class="container">
    <div class="row">
                    <div class="col-lg-5 col-lg-offset-3 col-sm-6 col-xs-12">
                        <ul>
                            <li><a href="https://247virtualassistants.com/story.php"><i class="fa fa-group"></i></i> 24/7 Story</a></li>
                            <li><a href="https://247virtualassistants.com/blog"><i class="fa fa-book"></i> Read Our Blog</a></li>
                            <li><a href="https://247virtualassistants.com/login.php"><i class="fa fa-sign-in"></i></i> Login</a></li>

                        </ul>

                    </div>
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                    	<span class="phone_no"><!--+1 267 632 6605-->+1  678 701 3467</span>
                    	<span class="callback"><a href="callback_form.php" id="call_back" class="fancybox fancybox.iframe">Request a callback</a></span></div>
                    
                </div>
    </div>
  </div>
<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>          </button>
          <a href="index.php" class="navbar-brand"><img src="images/logo.png" alt=""></a>        </div>
          <div class="menu_wrap">
        <div class="collapse navbar-collapse" id="navbar">
          <ul class="nav navbar-nav">
            <li><a href="index.php">Home</a></li>
            <li ><a href="how.php">How it works</a></li>
            <li ><a href="what-we-can-do.php">What We Can Do</a></li>
            <li ><a href="virtual-assistant-plans-for-business.php">Plans</a></li>
            <li ><a href="contact-virtual-assistant.php">Contact Us</a></li>
            <li class="lst_rgt"><a href="signup.php">  <button type="button" class="btn btn-primary">Sign up</button></a></li>
          </ul>
                

        </div><!--/.nav-collapse -->
        </div>
      </div>
  </nav>
<div class="top_wide">
<div class="container">
<div class="row text-center">
<h3>See what people are saying about VA!</h3>
</div>
</div>
</div>

<div class="testimonials_pad">
<div class="container">
<div class="row">
<div class="col-lg-7 col-xs-12 testbg"><img src="images/test1.png" alt="testimonials1"/></div>
<div class="col-lg-5 col-xs-12 testimonials"><p> I use 24/7 Virtual Assistants for my research & data entry.Working with these guys helps me take care of all the ground work so that I can focus on my business.</p>
<p>- Adam K</p></div>
</div>
</div>

<div class="container">
<div class="row ">
<div class="col-lg-5 col-xs-12 testimonials visible-lg visible-md"><p>The 24/7 team is so easy to deal with! I just email them what I need and I don't have to worry about it !</p>
<p>- Angie L</p></div>
<div class="col-lg-7 col-xs-12 testbg"><img src="images/test2.png" alt="testimonials2"/></div>
<div class="col-lg-5 col-xs-12 testimonials hidden-lg hidden-md"><p>The 24/7 team is so easy to deal with! I just email them what I need and I don't have to worry about it !</p>
<p>- Angie L</p></div>
</div>
</div>
</div>
 

 







<div class="socialmediabox">
    <div class="container">
        <div class="row text-center">
            <div class="socialmedia ">
                <h2>Let's Be Friends! Connect With Us</h2>
                <ul class="social_icon">
                    <li><a class="fb" href="https://www.facebook.com/247VirtualAssistant" target="_blank"></a></li>
                    <li><a class="twitter" href="https://twitter.com/247Assistants" target="_blank"></a></li>
                    <li><a class="gplus" href="https://plus.google.com/+247virtualassistant/posts" target="_blank"></a></li>
                    <li><a class="instagram" href="https://instagram.com/247virtualassistant/" target="_blank"></a></li>
                    <li><a class="pin" href="https://www.pinterest.com/247virtualassis/" target="_blank"></a></li>
                    <li><a class="linkedin" href="https://www.linkedin.com/profile/view?id=389064043&trk=nav_responsive_tab_profile" target="_blank"></a></li>
                    <li><a class="rss" href="https://247virtualassistants.com/blog/feed"  target="_blank"></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3"><a href="https://247virtualassistants.com"><img alt="" src="https://247virtualassistants.com/images/footerlogo.png"> </a><br>
                <p>Virtual assistant companies provide an efficient option for businesses that cannot afford teams of administrative assistants and in-house managers to outsource for skilled labor at cost effective charges. </p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
                <h3>Our Services</h3>
                <hr>
                <ul>
                    <li><a href="https://247virtualassistants.com/virtual-assistant.php">Virtual Assistant</a></li>
                    <li><a href="https://247virtualassistants.com/story.php">24/7 Virtual Assistant Story</a></li>
                    <li><a href="https://247virtualassistants.com/real-estate-virtual-assistant.php">Real Esate Virtual Assistant</a></li>
                    <li><a href="https://247virtualassistants.com/remote-assistant.php">Remote Assistant</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
                <h3>Informations</h3>
                <hr>
                <ul>
                    <li><a href="https://247virtualassistants.com/resource.php">Resources</a></li>
                    <li><a href="https://247virtualassistants.com/portfolio.php">Portfolio</a></li>
                    <li><a href="https://247virtualassistants.com/faq.php">Faq</a></li>
                    <li><a href="https://247virtualassistants.com/testimonials.php">Testimonials</a></li>
                    <li><a href="https://247virtualassistants.com/careers.php">Career</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
                <h3>Our Location</h3>
                <hr>
                <ul class="our_location">

                    <span><img alt="" src="https://247virtualassistants.com/images/route.png"></span><li>24/7 Virtual Assistant
                        1298 Braselton, Lawrenceville, GA 30043</li>

                    <span><img alt="" src="https://247virtualassistants.com/images/phone.png"></span><li>Tel: 678.701.3467</li>

                    <span><img alt="" src="https://247virtualassistants.com/images/fax.png"></span><li>Fax:(404)592-6523</li>
                </ul>
            </div>

        </div>
    </div>
</footer>
<script type='text/javascript'>
window.__wtw_lucky_site_id = 36691;

	(function() {
		var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
		wa.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://cdn') + '.luckyorange.com/w.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
	  })();
	</script>
	
	
<!-- begin olark code -->
<!--<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark || (function (c) {
        var f = window, d = document, l = f.location.protocol == "https:" ? "https:" : "http:", z = c.name, r = "load";
        var nt = function () {
            f[z] = function () {
                (a.s = a.s || []).push(arguments)
            };
            var a = f[z]._ = {
            }, q = c.methods.length;
            while (q--) {
                (function (n) {
                    f[z][n] = function () {
                        f[z]("call", n, arguments)
                    }
                })(c.methods[q])
            }
            a.l = c.loader;
            a.i = nt;
            a.p = {
                0: +new Date};
            a.P = function (u) {
                a.p[u] = new Date - a.p[0]
            };
            function s() {
                a.P(r);
                f[z](r)
            }
            f.addEventListener ? f.addEventListener(r, s, false) : f.attachEvent("on" + r, s);
            var ld = function () {
                function p(hd) {
                    hd = "head";
                    return["<", hd, "></", hd, "><", i, ' onl' + 'oad="var d=', g, ";d.getElementsByTagName('head')[0].", j, "(d.", h, "('script')).", k, "='", l, "//", a.l, "'", '"', "></", i, ">"].join("")
                }
                var i = "body", m = d[i];
                if (!m) {
                    return setTimeout(ld, 100)
                }
                a.P(1);
                var j = "appendChild", h = "createElement", k = "src", n = d[h]("div"), v = n[j](d[h](z)), b = d[h]("iframe"), g = "document", e = "domain", o;
                n.style.display = "none";
                m.insertBefore(n, m.firstChild).id = z;
                b.frameBorder = "0";
                b.id = z + "-loader";
                if (/MSIE[ ]+6/.test(navigator.userAgent)) {
                    b.src = "javascript:false"
                }
                b.allowTransparency = "true";
                v[j](b);
                try {
                    b.contentWindow[g].open()
                } catch (w) {
                    c[e] = d[e];
                    o = "javascript:var d=" + g + ".open();d.domain='" + d.domain + "';";
                    b[k] = o + "void(0);"
                }
                try {
                    var t = b.contentWindow[g];
                    t.write(p());
                    t.close()
                } catch (x) {
                    b[k] = o + 'd.write("' + p().replace(/"/g, String.fromCharCode(92) + '"') + '");d.close();'
                }
                a.P(2)
            };
            ld()
        };
        nt()
    })({
        loader: "static.olark.com/jsclient/loader0.js", name: "olark", methods: ["configure", "extend", "declare", "identify"]});
    /* custom configuration goes here (www.olark.com/documentation) */
    olark.identify('7843-714-10-6093');/*]]>*/</script><noscript><a href="https://www.olark.com/site/7843-714-10-6093/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>-->
<!-- end olark code -->
</body>
</html>