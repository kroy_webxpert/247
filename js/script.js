$.noConflict();
jQuery(document).ready(function (Jq) {
 
     
    jQuery('form#contactForm,#signup').validate({
        rules: {
            name:"required",
            lname : "required",
            fname : "required",
            pass : "required",
            mono : "required",
            confirmpass : "required",
            phone:{
                required: true,
                number:true
                },
            email:{
                required: true,
                email:true
                },
            message:"required",
            find_us:"required",
            
        },
        messages: {
            name:"Enter your name",
            phone:
            {
                required:"Enter your phone number",
                number:"Enter a valid number"
            },
            email:
            {
                required: "Enter your email id",
                email:"Enter a valid email id"
            },
            message:"Enter the message",
            find_us:"Enter how you find us",
            captcha: "Invalid captcha!"
        },
        
        
         onkeyup: false,
        
        
        
    }); 

      jQuery("form#captchaform").submit(function(e){
      e.preventDefault();

      var name = $("#name").val();
      var email = $("#email").val();
      var phone = $("#phone").val();
      var msg   = $("#msg").val();
      var errorMsg =0;
      $(".NameError,.EmailError,.PhoneError,.MsgError").html("");
      if(name=='')
      {
        $(".NameError").html('Enter Name').css('color','red');
        errorMsg++;
      }
      if(email=='')
      {
        $(".EmailError").html('Enter Email').css('color','red');
        errorMsg++;
      }
      if(phone=='')
      {
        $(".PhoneError").html('Enter Phone').css('color','red');
        errorMsg++;
      }
      if(msg=='')
      {
        $(".MsgError").html('Enter Message').css('color','red');
        errorMsg++;
      }
      if(errorMsg==0)
      {
         var data=$('#captchaform').serialize();
            $.ajax({
                type:'post',
                url:'consultantForm.php',
                data:data,
                beforeSend:function(){
                   // launchpreloader();
                },
                complete:function(){
                   // stopPreloader();
                },
                success:function(result){
                     alert(result);

                    $("#name").val('');
                    $("#email").val('');
                    $("#phone").val('');
                    $("#msg").val('');
                }
            });
      }
   // return false;
}); 

    var bg_video = document.getElementById('bg-vid');
    // Code that uses jQuery's $ can follow here.
   /* Jq('.fancybox-media').fancybox({
        openEffect: 'none',
        closeEffect: 'none',
        helpers: {
            media: {
            }
        },
        afterClose: function () {
            bg_video.play();
        },
        beforeLoad: function () {
            bg_video.pause();
        }
    });*/

  /*  Jq('#call_back').fancybox({
        'width': 400,
        'height': 600,
        'fitToView': true,
        'autoSize': false,
        'autoScale': false,
        'transitionIn': 'none',
        'transitionOut': 'none',
        'type': 'iframe'
    });*/
	/* Jq('#unsubs').fancybox({
        'width': 1000,
        'height': 650,
        'fitToView': true,
        'autoSize': false,
        'autoScale': false,
        'transitionIn': 'none',
        'transitionOut': 'none',
        'type': 'iframe'
    });
	  Jq('#free_consult').fancybox({
        'width': 700,
        'height': 410,
        'fitToView': true,
        'autoSize': false,
        'autoScale': false,
        'transitionIn': 'none',
        'transitionOut': 'none',
        'type': 'iframe'
    });*/
});

function getDetails(box)
{
    $('#we_care_4u').hide();
    jQuery('.reach_out_us').hide();
    jQuery('.get_introduce').hide();
    jQuery('.sign_up').hide();
    jQuery('.get_start').hide();

    if(box=='box1')
    {
        jQuery('.reach_out_us').slideDown();
        
    }
     if(box=='box2')
    {
        jQuery('.get_introduce').slideDown();
    }
     if(box=='box3')
    {
        jQuery('.sign_up').slideDown();
    }
     if(box=='box4')
    {
        jQuery('.get_start').slideDown();
    }
}
 

function do_check(email)
{
   var data = email; 
  $.ajax({
          type:'post',
          url:'send_mail.php?checkEmail=mail',
          data:{email:data},
          beforeSend:function(){
             // launchpreloader();
          },
          complete:function(){
             // stopPreloader();
          },
          success:function(result){
               $('#emailMsg').html(result);
              
          }
      });
}