<!DOCTYPE html>
<html lang="en">
    <head>
        <link href="https://plus.google.com/110412304848758494023" rel="publisher">
        <meta name="google-site-verification" content="0gfTllPwir_ODw8WbpLfTlCxYKeAOlwjPM34Z0TEmmQ" />
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="GDS">
        <meta name="Copyright" content="Copyright 2015 247VirtualAssistant">
        <meta name="Content-Language" content="en-us">
        <meta name="description" content="247 Virtual Assistant is a virtual assistant service that works for you. We give our clients access to one of the most capable and dedicated teams of Virtual Assistant not only in the country, but the entire planet.">
        <meta name="keywords" content="virtual assistant, virtual assistants">
        <title>247 Virtual Assistant</title>
                <!-- Bootstrap -->
        <link href="https://247virtualassistants.com/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <!--<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">-->

        <link href="css/styles.css" rel="stylesheet">
        <link href="https://247virtualassistants.com/css/styles_dev.css" rel="stylesheet">
        <link href="https://247virtualassistants.com/css/mediaqueries.css" rel="stylesheet">
        <link href="https://247virtualassistants.com/fonts/stylesheet.css" rel="stylesheet">
        <link href='//fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,200,600' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,200,600,700,900' rel='stylesheet' type='text/css'>
        <!--affiliate-->
        <link type="text/css" rel="stylesheet" href="https://247virtualassistants.com/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
        <script type="text/javascript" src="https://247virtualassistants.com/dhtmlgoodies_calendar1.js?random=20060118"></script>
        <script language="javascript" src="https://247virtualassistants.com/js/validates.js"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="https://247virtualassistants.com/js/bootstrap.min.js"></script>
        <script src="https://247virtualassistants.com/js/responsive-tabs.js"></script>
<!--        <script src="lib/jquery.js"></script>-->
        <script src="https://247virtualassistants.com/dist/jquery.validate.js"></script>
      <!--  <script src="https://247virtualassistants.com/captcha.js"></script>-->
                <!-- Add jQuery library -->
<script type="text/javascript" src="https://code.jquery.com/jquery-latest.min.js"></script>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="https://247virtualassistants.com/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

<!-- Add fancyBox -->
<link rel="stylesheet" href="https://247virtualassistants.com/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="https://247virtualassistants.com/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>

<!-- Optionally add helpers - button, thumbnail and/or media -->
<link rel="stylesheet" href="https://247virtualassistants.com/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="https://247virtualassistants.com/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="https://247virtualassistants.com/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

<link rel="stylesheet" href="https://247virtualassistants.com/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="https://247virtualassistants.com/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
<script src="https://www.youtube.com/player_api"></script>
<script type="text/javascript" src="https://247virtualassistants.com/js/script.js"></script>
<!--<script type="text/javascript" src="http://192.168.1.50/virtualassistant/247virtualassistant/js/script.js"></script>-->    </head><body>
                <div class="sliderform_base">
                    <span>FREE Consultation</span>
                    <p>Note: Ask us how to get 10% off for your first month!                     </p>

                    <form name="" id="f_consult" method="post"   action="consult_pop.php">
                        <div class="form-group">
                            <input type="text" class="form-control" name="name" placeholder="Name">
                        </div>
                        <div class="form-group">
                            <input type="email" class="form-control" name="email" placeholder="E-mail">
                        </div>
                        <div class="form-group">
                            <input type="tel" class="form-control" name="phone" placeholder="Phone no">
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" rows="3" name="message" placeholder="What can we do for you?"></textarea>
                        </div>
                        <div class="form-group">
                            <input type="text" class="form-control capcha_field" name="code" id="code" placeholder="Enter Code">
                            <input name="validate" type="hidden"  class="input_control" value="validate">
                            <div><img src="includes/rand.php"  ></div>
                        </div>  <input type="submit"  class="submit" value="Send">

                    </form>
            </div>
			<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.js"></script>
<script type="text/javascript">
    $(document).ready(function (e) {
        $("#f_consult").validate({
            errorClass: 'error_msgs',
            rules: {
                name: "required",
                phone: {
                    required: true,
                    number: true,
                    remote: "phone_validation.php"
                },
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                name: "Enter your name",
                phone:
                        {
                            required: "Enter your phone number",
                            number: "Enter a valid phone number",
                        },
			    email:
					    {
                        required: "Enter your email address",
                        email: "Enter a valid email address"
                }
            },
            onkeyup: false,
        });

    });

</script> 
</body>
</html>
