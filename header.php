<!DOCTYPE html>
<html lang="en">
    <head>
         <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="GDS">
        <meta name="Copyright" content="Copyright 2015 247VirtualAssistant">
        <meta name="Content-Language" content="en-us">
        <meta name="description" content="Got a Question? Let one of Our virtual assistants help!">
         
        <title>Pelicanhighway</title>
                <!-- Bootstrap -->
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="fonts/font-awesome.min.css">
        <!--<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">-->

        <link href="css/styles.css" rel="stylesheet">
        <link href="css/styles_dev.css" rel="stylesheet">
        <link href="css/mediaqueries.css" rel="stylesheet">
        <link href="fonts/stylesheet.css" rel="stylesheet">
         <link href='//fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,200,600' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,200,600,700,900' rel='stylesheet' type='text/css'>
		<link href='https://fonts.googleapis.com/css?family=Lato:400,300,700,900,100' rel='stylesheet' type='text/css'>
          <!--affiliate-->
        <link type="text/css" rel="stylesheet" href="dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
        <script type="text/javascript" src="dhtmlgoodies_calendar1.js?random=20060118"></script>
        
        <script src="js/jquery-1.11.2.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="js/bootstrap.min.js" type="text/javascript" ></script>
 
        <script src="js/responsive-tabs.js"></script>
 
        <script src="js/jquery-latest.min.js"></script>
 
<!-- <script type="text/javascript" src="fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
 -->
<!-- Add fancyBox -->
<!-- <link rel="stylesheet" href="fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>
 -->
<!-- Optionally add helpers - button, thumbnail and/or media -->
<!-- <link rel="stylesheet" href="fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

<link rel="stylesheet" href="fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
 --><script src="https://www.youtube.com/player_api"></script>

<script type="text/javascript" src="js/script.js"></script>
<!--<script type="text/javascript" src="http://192.168.1.50/virtualassistant/247virtualassistant/js/script.js"></script>-->   
</head>

<body>