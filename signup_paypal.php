<!DOCTYPE html>
<html lang="en">
    <head>
        <link href="https://plus.google.com/110412304848758494023" rel="publisher">
        <meta name="google-site-verification" content="0gfTllPwir_ODw8WbpLfTlCxYKeAOlwjPM34Z0TEmmQ" />
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="GDS">
        <meta name="Copyright" content="Copyright 2015 247VirtualAssistant">
        <meta name="Content-Language" content="en-us">
        <meta name="description" content="Virtual assistant plans for business">
        <meta name="keywords" content="virtual secretary, virtual administrative assistant, virtual executive assistant">
        <title>247 Virtual Assistant - Offshore And US Plans</title>
                <!-- Bootstrap -->
        <link href="https://247virtualassistants.com/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <!--<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">-->

        <link href="css/styles.css" rel="stylesheet">
        <link href="https://247virtualassistants.com/css/styles_dev.css" rel="stylesheet">
        <link href="https://247virtualassistants.com/css/mediaqueries.css" rel="stylesheet">
        <link href="https://247virtualassistants.com/fonts/stylesheet.css" rel="stylesheet">
        <link href='//fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,200,600' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,200,600,700,900' rel='stylesheet' type='text/css'>
        <!--affiliate-->
        <link type="text/css" rel="stylesheet" href="https://247virtualassistants.com/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
        <script type="text/javascript" src="https://247virtualassistants.com/dhtmlgoodies_calendar1.js?random=20060118"></script>
        <script language="javascript" src="https://247virtualassistants.com/js/validates.js"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="https://247virtualassistants.com/js/bootstrap.min.js"></script>
        <script src="https://247virtualassistants.com/js/responsive-tabs.js"></script>
<!--        <script src="lib/jquery.js"></script>-->
        <script src="https://247virtualassistants.com/dist/jquery.validate.js"></script>
      <!--  <script src="https://247virtualassistants.com/captcha.js"></script>-->
                <!-- Add jQuery library -->
<script type="text/javascript" src="https://code.jquery.com/jquery-latest.min.js"></script>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="https://247virtualassistants.com/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

<!-- Add fancyBox -->
<link rel="stylesheet" href="https://247virtualassistants.com/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="https://247virtualassistants.com/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>

<!-- Optionally add helpers - button, thumbnail and/or media -->
<link rel="stylesheet" href="https://247virtualassistants.com/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="https://247virtualassistants.com/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="https://247virtualassistants.com/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

<link rel="stylesheet" href="https://247virtualassistants.com/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="https://247virtualassistants.com/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
<script src="https://www.youtube.com/player_api"></script>
<script type="text/javascript" src="https://247virtualassistants.com/js/script.js"></script>
<!--<script type="text/javascript" src="http://192.168.1.50/virtualassistant/247virtualassistant/js/script.js"></script>-->    </head>    <body>
        <div class="top">
            <div class="banner_device"><img src="images/banner_subdevice.png" alt="virtual-assistance-plans"></div>
            <div class="banner_sub_plans">
                <div class="container">
                    <div class="col-lg-12">
                        <div class="bannertext ">

                            <span><img alt="" src="images/leftborder.png" alt="leftborder"></span> <h3>PLANS</h3><span><img alt="rightborder" src="images/rightborder.png"></span>
                        </div>
                    </div>

                </div>
            </div>
        </div>

          <div class="container-top">
  <div class="container">
    <div class="row">
                    <div class="col-lg-5 col-lg-offset-3 col-sm-6 col-xs-12">
                        <ul>
                            <li><a href="https://247virtualassistants.com/story.php"><i class="fa fa-group"></i></i> 24/7 Story</a></li>
                            <li><a href="https://247virtualassistants.com/blog"><i class="fa fa-book"></i> Read Our Blog</a></li>
                            <li><a href="https://247virtualassistants.com/login.php"><i class="fa fa-sign-in"></i></i> Login</a></li>

                        </ul>

                    </div>
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                    	<span class="phone_no"><!--+1 267 632 6605-->+1  678 701 3467</span>
                    	<span class="callback"><a href="callback_form.php" id="call_back" class="fancybox fancybox.iframe">Request a callback</a></span></div>
                    
                </div>
    </div>
  </div>
<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>          </button>
          <a href="index.php" class="navbar-brand"><img src="images/logo.png" alt=""></a>        </div>
          <div class="menu_wrap">
        <div class="collapse navbar-collapse" id="navbar">
          <ul class="nav navbar-nav">
            <li><a href="index.php">Home</a></li>
            <li ><a href="how.php">How it works</a></li>
            <li ><a href="what-we-can-do.php">What We Can Do</a></li>
            <li  class ="active" ><a href="virtual-assistant-plans-for-business.php">Plans</a></li>
            <li ><a href="contact-virtual-assistant.php">Contact Us</a></li>
            <li class="lst_rgt"><a href="signup.php">  <button type="button" class="btn btn-primary">Sign up</button></a></li>
          </ul>
                

        </div><!--/.nav-collapse -->
        </div>
      </div>
  </nav>
        <div class="top_wide">
            <div class="container">
                <div class="row text-center">
                    <h3>Try one of our Virtual Assistants. Risk free and available when you need them<br>Need Help?			</h3>
					<a href="freeconsult_form.php" id="free_consult" class="fancybox fancybox.iframe"><button class="btn btn-primary">Free Consultation</button></a>
                    
                </div>
            </div>
        </div>

        <div class="clearfix"></div>


        <div class="greybg base">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="plan_container">

                            <ul class="nav nav-tabs responsive" id="myTab">

                                <li class="test-class active"><a href="#features"><span class="offshore_icon"><img src="images/offshore.png" alt="offshore"></span>Offshore Plans</a></li>   
                                <li class="test-class "><a class="deco-none misc-class" href="#how-to"><span class="us_icon"><img src="images/us_icon.png" alt="us_icon"></span>US Plans</a></li>        </ul>

                            <div class="tab-content responsive">
                                <div class="tab-pane" id="how-to">
                                    <div class="container no-padding">
                                        <div class="pricing-table pricing-three-column row">
                                            <div class="plan col-sm-5 col-xs-5 col-lg-3">
                                                <a href="signup.php?type=76&signup=yes&plan=BUE"> <button type="button" class="btn btn-default btn-circle_blue">$359</button></a>
                                                <h3>Entrepreneur <br><br>

                                                    <span>Monthly Membership
                                                        24/7 Assistance<br>
                                                        20 hours/ month</span></h3>

                                                <a href="signup.php?type=76&signup=yes&plan=BUE"> <button type="button" class="btn btn-success">SIGNUP NOW</button></a>                
                                            </div>


                                            <div style="z-index:55;" class="plan col-sm-5 col-xs-5 col-lg-3">
                                                <a href="signup.php?type=77&signup=yes&plan=BUE"> <button type="button" class="btn btn-default btn-circle_green">$699</button></a>
                                                <h3>Starter <br><br>                
                                                    <span>Monthly Membership
                                                        24/7 Assistance<br>
                                                        40 hours/ month</span></h3>
                                                <a href="signup.php?type=77&signup=yes&plan=BUE"> <button type="button" class="btn btn-success">SIGNUP NOW</button></a>
                                            </div>


                                            <div class="plan col-sm-5 col-xs-5 col-lg-3">
                                                <a href="signup.php?type=78&signup=yes&plan=BUE"> <button type="button" class="btn btn-default btn-circle_cyan">$999</button></a>
                                                <h3>Professional 
                                                    <br><br>                
                                                    <span>Monthly Membership
                                                        24/7 Assistance<br>
                                                        60 hours/ month</span></h3>                
                                                <a href="signup.php?type=78&signup=yes&plan=BUE"> <button type="button" class="btn btn-success">SIGNUP NOW</button></a>
                                            </div>

                                            <div class="plan col-sm-5 col-xs-5 col-lg-3">
                                                <a href="signup.php?type=79&signup=yes&plan=BUE">  <button type="button" class="btn btn-default btn-circle_cyan">$1299</button></a>
                                                <h3>Office <br><br>

                                                    <span>Monthly Membership
                                                        24/7 Assistance<br>
                                                        80 hours/ month</span></h3>
                                                <a href="signup.php?type=79&signup=yes&plan=BUE"> <button type="button" class="btn btn-success">SIGNUP NOW</button></a>               
                                            </div>


                                            <div style="z-index:55;" class="plan col-sm-5 col-xs-5 col-lg-3">
                                                <a href="signup.php?type=80&signup=yes&plan=BUE"> <button type="button" class="btn btn-default btn-circle_green">$1799</button></a>
                                                <h3>Executive <br><br>

                                                    <span>Monthly Membership
                                                        24/7 Assistance<br>
                                                        120 hours/ month</span></h3>
                                                <a href="signup.php?type=80&signup=yes&plan=BUE"> <button type="button" class="btn btn-success">SIGNUP NOW</button></a>
                                            </div>
                                            <div class="plan col-sm-5 col-xs-5 col-lg-3">
                                                <a href="signup.php?type=81&signup=yes&plan=BUE">  <button type="button" class="btn btn-default btn-circle_blue">$2399</button></a>
                                                <h3>VIP <br><br>

                                                    <span>Monthly Membership
                                                        24/7 Assistance<br>
                                                        160 hours/ month</span></h3>
                                                <a href="signup.php?type=81&signup=yes&plan=BUE"> <button type="button" class="btn btn-success">SIGNUP NOW</button></a>
                                            </div>


                                        </div>
                                    </div>

                                </div>
                                <div class="tab-pane active" id="features">

                                    <div class="container no-padding">
                                        <div class="pricing-table pricing-three-column row">
                                            <div class="plan col-sm-5 col-xs-5 col-lg-3">
                                                <a href="signup.php?type=70&signup=yes&plan=BUE"> <button type="button" class="btn btn-default btn-circle_blue">$299</button></a>
                                                <h3>Entrepreneur<br><br>

                                                    <span>Monthly Membership
                                                        24/7 Assistance<br>
                                                        30 hours/ month</span></h3>

                                                <a href="signup.php?type=70&signup=yes&plan=BUE"> <button type="button" class="btn btn-success">SIGNUP NOW</button></a>

                                            </div>
                                            <div style="z-index:55;" class="plan col-sm-5 col-xs-5 col-lg-3">
                                                <a href="signup.php?type=71&signup=yes&plan=BUE"> <button type="button" class="btn btn-default btn-circle_green">$499</button></a>
                                                <h3>Professional<br><br>

                                                    <span>Monthly Membership
                                                        24/7 Assistance<br>
                                                        60 hours/ month</span></h3>

                                                <a href="signup.php?type=71&signup=yes&plan=BUE" rel="nofollow">  <button type="button" class="btn btn-success">SIGNUP NOW</button></a>
                                            </div>
                                            <div class="plan col-sm-5 col-xs-5 col-lg-3">
                                                <a href="signup.php?type=72&signup=yes&plan=BUE"><button type="button" class="btn btn-default btn-circle_blue">$799</button></a>
                                                <h3>Executive<br><br>

                                                    <span>Monthly Membership
                                                        24/7 Assistance<br>
                                                        120 hours/ month</span></h3>

                                                <a href="signup.php?type=72&signup=yes&plan=BUE"><button type="button" class="btn btn-success">SIGNUP NOW</button></a>
                                            </div>            

                                            <div class="plan col-sm-5 col-xs-5 col-lg-3">
                                                <a href="signup.php?type=73&signup=yes&plan=BUE"><button type="button" class="btn btn-default btn-circle_blue">$999</button></a>
                                                <h3>VIP<br><br>

                                                    <span>Monthly Membership
                                                        24/7 Assistance<br>
                                                        160 hours/ month</span></h3>

                                                <a href="signup.php?type=73&signup=yes&plan=BUE"><button type="button" class="btn btn-success">SIGNUP NOW</button></a>
                                            </div>
                                            <div class="no-plan col-sm-5 col-xs-5 col-lg-3"></div>

                                            <div class="plan col-sm-5 col-xs-5 col-lg-3">
                                                <a href="bythehoursignup.php"><button type="button" class="btn btn-default btn-circle_green">$15</button></a>
                                                <h3>By the Hour<br><br>

                                                    <span>Hourly Membership
                                                        24/7 Assistance<br>
                                                        $15/ hour</span></h3>

                                                <a href="bythehoursignup.php"><button type="button" class="btn btn-success">SIGNUP NOW</button></a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12"><div class="conditions"><p>*Conditions Applied. Our 24/7 Plans may vary as per the terms and conditions.<br>

                                You can check our plans policy and its terms and conditions <a href="tos.php">Here</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="plan_bg01 text-center">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12 pt_100">
                        <a class="box_link" href="how.php">PLANS AND FEATURES</a>
                    </div>
                </div>
            </div>
        </div>



        <div class="container base">
            <div class="row iconhead ">

                <div class="col-lg-4"><img src="images/va_icon01.png" alt="virtual-assistance-va_icon01"/>
                    <h3>Why choose our Virtual Assistants?</h3>
                    <div class="clearfix"></div>
                    <p class="content iconhead_pad">Many companies providing virtual assistant services will most often charge you premium rate but quite possibly under-deliver in terms of quality. Others provide lower prices but only for clients signing up for long term contracts. Ours is a very different approach. We provide you with time and service-specific virtual assistants business plans aimed at giving you value for your money. This allows you to choose a specific virtual assistant plan that exactly suits your personal and company needs. You can always rely on the quality of service provided by our highly trained virtual assistants who are proficient in MS Office, Open Office, Google Tools, and a host of other tools a personal assistant should be well versed in.  </p>
                </div>


                <div class="col-lg-4"><img src="images/va_icon02.png" alt="virtual-assistance-va_icon02"/>
                    <h3>What can our Virtual Assistant plans do?</h3>
                    <div class="clearfix"></div>
                    <p class="content iconhead_pad">Our virtual assistants teams are specialists in</p>
                    <ul class="arrowblt">
                    	<li>Administrative Support</li>
                        <li>Telephonic & Internet Support</li>
                        <li>Book Keeping</li>
                        <li>Customer Services</li>
                        <li>Data Mining</li>
                        <li>Email Marketing</li>
                        <li>Executive & Clerical Support</li>
                        <li>Human Resource</li>
                        <li>Inbound Sales</li>
                        <li>Live Support</li>
                        <li>Mailing List Development</li>
                        <li>Real Estate Services</li>
                        <li>Reports</li>
                        <li>Web Design</li>
                        <li>Web Research</li>
                        <li>Word Processing & Data Entry</li>
                    </ul>
                </div>


                <div class="col-lg-4"><img src="images/va_icon03.png" alt="virtual-assistance-va_icon03"/>
                    <h3>How flexible is our VA plans?</h3>
                    <div class="clearfix"></div>
                    <p class="content iconhead_pad">All our virtual assistant plans are designed to suit the needs of any type of client. This is why each plan comes in a month to month contract that is simply an agreement to our Terms and Conditions. We strive to avoid unnecessary complications in all our contracts. The simplicity of our contracts makes them quite flexible allowing you to upgrade, downgrade, renew, or even cancel the contract whenever you wish. Your virtual assistants will quite simply take care of everything when the month ends. If you would like to know more about our services, feel free to talk to one of our managers or even a virtual assistant any time.	 </p>
                </div>
            </div>
        </div>

        <div class="top_wide">
            <div class="container">
                <div class="row text-center light">
                    <h2>Not sure which plan works for you? <br> Give us a call and we can help!<span style="color: #3ea9f5"> 678-701-3467 </span></h2>
                </div>
            </div>
        </div>

<!--        <div class="container plan_btm_cont">
            <div class="row base">

                <div class="col-lg-6">
                    <h3>Virtual Assistant Plans offered at
                        247VirtualAssistants.com</h3>
                    <div class="content">
                        <p>We have five types of virtual assistant plans designed to meet every client's budgetary and service needs. Each plan addresses the amount of time you may need to use our services each month, of course with an option to upgrade as your business grows. These plans are Entreprenuer, Starter, Professional, Office, Executive and VIP plans. </p>
                    </div>
                </div>

                <div class="col-lg-6 text-right"><img src="images/lap.png" alt="virtual-assistance-lap"/></div>
            </div>
        </div>-->


        <div class="socialmediabox">
    <div class="container">
        <div class="row text-center">
            <div class="socialmedia ">
                <h2>Let's Be Friends! Connect With Us</h2>
                <ul class="social_icon">
                    <li><a class="fb" href="https://www.facebook.com/247VirtualAssistant" target="_blank"></a></li>
                    <li><a class="twitter" href="https://twitter.com/247Assistants" target="_blank"></a></li>
                    <li><a class="gplus" href="https://plus.google.com/+247virtualassistant/posts" target="_blank"></a></li>
                    <li><a class="instagram" href="https://instagram.com/247virtualassistant/" target="_blank"></a></li>
                    <li><a class="pin" href="https://www.pinterest.com/247virtualassis/" target="_blank"></a></li>
                    <li><a class="linkedin" href="https://www.linkedin.com/profile/view?id=389064043&trk=nav_responsive_tab_profile" target="_blank"></a></li>
                    <li><a class="rss" href="https://247virtualassistants.com/blog/feed"  target="_blank"></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3"><a href="https://247virtualassistants.com"><img alt="" src="https://247virtualassistants.com/images/footerlogo.png"> </a><br>
                <p>Virtual assistant companies provide an efficient option for businesses that cannot afford teams of administrative assistants and in-house managers to outsource for skilled labor at cost effective charges. </p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
                <h3>Our Services</h3>
                <hr>
                <ul>
                    <li><a href="https://247virtualassistants.com/virtual-assistant.php">Virtual Assistant</a></li>
                    <li><a href="https://247virtualassistants.com/story.php">24/7 Virtual Assistant Story</a></li>
                    <li><a href="https://247virtualassistants.com/real-estate-virtual-assistant.php">Real Esate Virtual Assistant</a></li>
                    <li><a href="https://247virtualassistants.com/remote-assistant.php">Remote Assistant</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
                <h3>Informations</h3>
                <hr>
                <ul>
                    <li><a href="https://247virtualassistants.com/resource.php">Resources</a></li>
                    <li><a href="https://247virtualassistants.com/portfolio.php">Portfolio</a></li>
                    <li><a href="https://247virtualassistants.com/faq.php">Faq</a></li>
                    <li><a href="https://247virtualassistants.com/testimonials.php">Testimonials</a></li>
                    <li><a href="https://247virtualassistants.com/careers.php">Career</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
                <h3>Our Location</h3>
                <hr>
                <ul class="our_location">

                    <span><img alt="" src="https://247virtualassistants.com/images/route.png"></span><li>24/7 Virtual Assistant
                        1298 Braselton, Lawrenceville, GA 30043</li>

                    <span><img alt="" src="https://247virtualassistants.com/images/phone.png"></span><li>Tel: 678.701.3467</li>

                    <span><img alt="" src="https://247virtualassistants.com/images/fax.png"></span><li>Fax:(404)592-6523</li>
                </ul>
            </div>

        </div>
    </div>
</footer>
<script type='text/javascript'>
window.__wtw_lucky_site_id = 36691;

	(function() {
		var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
		wa.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://cdn') + '.luckyorange.com/w.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
	  })();
	</script>
	
	
<!-- begin olark code -->
<!--<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark || (function (c) {
        var f = window, d = document, l = f.location.protocol == "https:" ? "https:" : "http:", z = c.name, r = "load";
        var nt = function () {
            f[z] = function () {
                (a.s = a.s || []).push(arguments)
            };
            var a = f[z]._ = {
            }, q = c.methods.length;
            while (q--) {
                (function (n) {
                    f[z][n] = function () {
                        f[z]("call", n, arguments)
                    }
                })(c.methods[q])
            }
            a.l = c.loader;
            a.i = nt;
            a.p = {
                0: +new Date};
            a.P = function (u) {
                a.p[u] = new Date - a.p[0]
            };
            function s() {
                a.P(r);
                f[z](r)
            }
            f.addEventListener ? f.addEventListener(r, s, false) : f.attachEvent("on" + r, s);
            var ld = function () {
                function p(hd) {
                    hd = "head";
                    return["<", hd, "></", hd, "><", i, ' onl' + 'oad="var d=', g, ";d.getElementsByTagName('head')[0].", j, "(d.", h, "('script')).", k, "='", l, "//", a.l, "'", '"', "></", i, ">"].join("")
                }
                var i = "body", m = d[i];
                if (!m) {
                    return setTimeout(ld, 100)
                }
                a.P(1);
                var j = "appendChild", h = "createElement", k = "src", n = d[h]("div"), v = n[j](d[h](z)), b = d[h]("iframe"), g = "document", e = "domain", o;
                n.style.display = "none";
                m.insertBefore(n, m.firstChild).id = z;
                b.frameBorder = "0";
                b.id = z + "-loader";
                if (/MSIE[ ]+6/.test(navigator.userAgent)) {
                    b.src = "javascript:false"
                }
                b.allowTransparency = "true";
                v[j](b);
                try {
                    b.contentWindow[g].open()
                } catch (w) {
                    c[e] = d[e];
                    o = "javascript:var d=" + g + ".open();d.domain='" + d.domain + "';";
                    b[k] = o + "void(0);"
                }
                try {
                    var t = b.contentWindow[g];
                    t.write(p());
                    t.close()
                } catch (x) {
                    b[k] = o + 'd.write("' + p().replace(/"/g, String.fromCharCode(92) + '"') + '");d.close();'
                }
                a.P(2)
            };
            ld()
        };
        nt()
    })({
        loader: "static.olark.com/jsclient/loader0.js", name: "olark", methods: ["configure", "extend", "declare", "identify"]});
    /* custom configuration goes here (www.olark.com/documentation) */
    olark.identify('7843-714-10-6093');/*]]>*/</script><noscript><a href="https://www.olark.com/site/7843-714-10-6093/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>-->
<!-- end olark code -->

        <script type="text/javascript">
            $('#myTab a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });

            $('#moreTabs a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });

            (function ($) {
                // Test for making sure event are maintained
                $('.js-alert-test').click(function () {
                    alert('Button Clicked: Event was maintained');
                });
                fakewaffle.responsiveTabs(['xs', 'sm']);
            })(jQuery);

        </script>
        <script>
            (function (i, s, o, g, r, a, m) {
                i['GoogleAnalyticsObject'] = r;
                i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
                a = s.createElement(o),
                        m = s.getElementsByTagName(o)[0];
                a.async = 1;
                a.src = g;
                m.parentNode.insertBefore(a, m)
            })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

            ga('create', 'UA-17600125-2', 'openam.github.io');
            ga('send', 'pageview');
        </script>

    </body>
</html>