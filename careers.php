<?php include('header.php'); ?>

<div class="top">
<div class="banner_device"><img src="images/banner_subdevice.png" alt="virtual-assistance-careers"/></div>
  <div class="banner_sub_remote">
  <div class="container">
  <div class="col-lg-12">
  <div class="bannertext ">
    
  <span><img src="images/leftborder.png" alt="leftborder"/></span> <h3>Pelican Highway Career</h3><span><img src="images/rightborder.png" alt="rightborder"/></span>
  </div>
  </div>
  
  </div>
  </div>
</div>
  
<?php include('top.php'); ?>

<div class="top_wide">
<div class="container">
<div class="row text-center">
<h3>Looking for a Career? You came to the right place!</h3>
</div>
</div>
</div>

<div class="container">
<div class="row base">
<div class="col-lg-6 col-xs-6"><img src="images/career.png" alt="careers" /></div>
<div class="col-lg-6 career_pad">
<h3>Work With Pelican Highway Virtual Assistant</h3>
<div class="content">
<p>We are one of the top providing virtual assisting companies in the world. We promote team collaboration, and make sure everyone who comes to work feels they are an important piece of our success.</p></br>
<h3>Apply</h3>
<p>If you would like to join our team , please send your resume to  <a href="mailto:careers@pelicanhighway.com">careers@pelicanhighway.com</a>. If you are fluent in any other languages other than English, make sure to include that also in your resume since we have clients from all over the world.  Our Human Resource Department will go through your resume as soon as it comes through . If we have an immediate client need that matches your skill sets, we  will contact you right away. If not, we will keep your resume on file and contact you when we have a need that matches your skills to see if you are available at that time. Thank you for showing interest in working with our company and we look forward to having you as part of our team. </p>
 
</div>
</div>
</div>
</div>

 
<?php include('footer.php'); ?>
