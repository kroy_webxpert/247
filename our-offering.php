<?php 
  include 'header.php';
?>
<div class="top">
        <div class="banner_device"><img src="images/banner_subdevice.png" alt="virtual-assistance-hire"/></div>
        <div class="banner_sub">
            <div class="container">
                <div class="col-lg-12">
                    <div class="bannertext ">

                        <span><img src="images/leftborder.png" alt="leftborder"/></span> <h3>Our Offering</h3>
                        <span><img src="images/rightborder.png" alt="rightborder"/></span>
                    </div>
                </div>

            </div>
        </div>
    </div>

<?php 
  include 'top.php';
?>

  <div class="top_wide">
        <div class="container">
            <div class="row text-center">
                <h3>Our Offering</h3>
               <h5 style="margin-top: 10px; color: rgb(143, 139, 149);">
                  Need an efficient and sharp executive assistant who can get your schedule organized and handle your day to day activities? Or do you need help with your professional and technical requirement. Let us know what you need and choose from the wide array of our services. 
                </h5>
            </div>
        </div>
    </div>

    <div class="container">
      <div class="row base">
           <div class="col-md-12">
             <div class="col-md-3"><img src="img/admin.png">
               <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingOne">
      <h4 class="panel-title">
        <a data-toggle="collapse" class="collapsed" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
         
        Administrative 
        </a>
      </h4>
    </div>
    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
        
      <div class="panel-body">
        <ul>
          <li><a href="#">Email management
</a></li>
          <li><a href="#">Calendar Management
</a></li>
          <li><a href="#">Appointment setting
</a></li>
          <li><a href="#">Book keeping
</a></li>
          <li><a href="#">Event planning
</a></li>
          <li><a href="#">Call screening
</a></li>
          <li><a href="#">To do list management
</a></li>
          
        </ul>
         
      </div>
    </div>
  </div>


             </div>
              <div class="col-md-3"><img src="img/research.png">
               
   
 
 
  
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingFifteen">
      <h4 class="panel-title">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFifteen" aria-expanded="false" aria-controls="collapseFifteen">
            Research 
        </a>
      </h4>
    </div>
    <div id="collapseFifteen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFifteen">
      <div class="panel-body">
        <ul>
          <li><a href="#">Web Research

</a></li>
          <li><a href="#">Data Entry

</a></li>
          <li><a href="#">Charts


</a></li>
          <li><a href="#">Charts
</a></li>
          
          
        </ul>
       </div>
    </div>
  </div>
             </div>
              <div class="col-md-3"><img src="img/travel_mangmnt.png">
              <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingNine">
      <h4 class="panel-title">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseNine" aria-expanded="false" aria-controls="collapseNine">
         Travel Management
        </a>
      </h4>
    </div>
    <div id="collapseNine" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingNine">
     <div class="panel-body">
        <ul>
          <li><a href="#">Ticketing
</a></li>
          <li><a href="#">Cancellations
</a></li>
          <li><a href="#">Hotel booking

</a></li>
          <li><a href="#">Itineraries
</a></li>
          <li><a href="#">Visas
          <li><a href="#">Travel insurances etc.

</a></li>
         
          
        </ul>
       </div>
    </div>
  </div>
             </div>
              <div class="col-md-3"><img src="img/personel.png">
                <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingTen">
      <h4 class="panel-title">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTen" aria-expanded="false" aria-controls="collapseTen">
           Personel
        </a>
      </h4>
    </div>
    <div id="collapseTen" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTen">
      <div class="panel-body">
        <ul>
          <li><a href="#">Online shopping

</a></li>
          <li><a href="#">Reservations
</a></li>
          <li><a href="#">Personal appointments

</a></li>
          <li><a href="#">Assist with customer 
</a></li>
          <li><a href="#">service </a></li>
           
          
        </ul>
       </div>
    </div>
  </div>
  
             </div>

          </div>

          <div class="col-md-12" style="margin-top: 30px;">
             <div class="col-md-3"><img src="img/hardware_software.png">
                
  <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingFifteen1">
      <h4 class="panel-title">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFifteen1" aria-expanded="false" aria-controls="collapseFifteen1">
            Hardware/Software support
        </a>
      </h4>
    </div>
   <!--  <div id="collapseFifteen1" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFifteen1">
      <div class="panel-body">
         Hardware & Software support
       </div>
    </div> -->
  </div>
 
             </div>
              <div class="col-md-3"><img src="img/web_graphic.png">
             
              <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingFifteen2">
      <h4 class="panel-title">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFifteen2" aria-expanded="false" aria-controls="collapseFifteen2">
             Web and Graphic Design
        </a>
      </h4>
    </div>
    <!-- <div id="collapseFifteen2" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFifteen2">
      <div class="panel-body">
        Web and Graphic Design
       </div>
    </div> -->
  </div>
 
             </div>
              <div class="col-md-3"><img src="img/seo.png">
               
                 <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingFifteen3">
      <h4 class="panel-title">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFifteen3" aria-expanded="false" aria-controls="collapseFifteen3">
            SEO
        </a>
      </h4>
    </div>
   <!--  <div id="collapseFifteen3" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFifteen3">
      <div class="panel-body">
       SEO
       </div>
    </div> -->
  </div>
             </div>
              <div class="col-md-3"><img src="img/other_adv_task.png">
              
              <div class="panel panel-default">
    <div class="panel-heading" role="tab" id="headingFifteen4">
      <h4 class="panel-title">
        <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseFifteen4" aria-expanded="false" aria-controls="collapseFifteen4">
           Other advanced tasks 
        </a>
      </h4>
    </div>
   <!--  <div id="collapseFifteen4" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFifteen4">
      <div class="panel-body">
        Other advanced tasks 
       </div>
    </div> -->
  </div>
         
             </div>

          </div>


        </div>
         
    </div>

    

    

    <?php 
  include 'footer.php';
?>