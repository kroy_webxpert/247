<?php include('header.php'); ?>
 <div class="top">
        <div class="banner_device"><img src="images/banner_subdevice.png" alt="virtual-assistance-hire"></div>
        <div class="banner_sub">
            <div class="container">
                <div class="col-lg-12">
                    <div class="bannertext ">

                        <span><img src="images/leftborder.png" alt="leftborder"></span> <h3>HOW WE SERVE</h3><span><img src="images/rightborder.png" alt="rightborder"></span>
                    </div>
                </div>

            </div>
        </div>
    </div>
   <?php include('top.php'); ?>

      <div class="top_wide">
        <div class="container">
            <div class="row text-center">
                <h3>Get started with four simple steps</h3>
            </div>
        </div>
    </div>

    <div class="container">
        <div class="row base">
            
             <div class="col-lg-12 col-xs-12" style="padding-bottom: 30px;">

                <div class="col-lg-3 col-xs-3 boxes">
                    <div class="services-nav">
                        <ul>
                            <li><a href="javascript:void(0)" onclick="getDetails('box1');"><i></i>
                                Reach out to us</a></li> 
                        </ul>
                    </div>

                     
                </div>
                <div class="col-lg-3 col-xs-3 boxes">
                    <div class="services-nav">
                        <ul>
                            <li><a href="javascript:void(0)" onclick="getDetails('box2');"><i></i>
                                Get introduced</a></li> 
                        </ul>
                    </div>
                    
</div>
                <div class="col-lg-3 col-xs-3 boxes">
                  <div class="services-nav">
                        <ul>
                            <li><a href="javascript:void(0)" onclick="getDetails('box3');"><i></i>
                                Sign Up</a></li> 
                        </ul>
                    </div>
                    
</div>

                <div class="col-lg-3 col-xs-3 boxes">
                   <div class="services-nav">
                        <ul>
                            <li><a href="javascript:void(0)" onclick="getDetails('box4');"><i></i>
                                Get tasks executed</a></li> 
                        </ul>
                    </div>
                    
</div>


             </div>
             <div class="col-lg-12 col-xs-12" style="width: 100%; margin: 15px; border: 1px dashed; padding: 20px 15px;" id="we_care_4u">
                 
               <p style="padding: 20px 0px; font-size: 25px; font-weight: 700;">  We care for you! </p>
               <p style="text-align: justify; line-height: 20px;"> 
We Pelicans strongly believe that your achievement demonstrates our success. Your VA will be your 24X7 hour friend to reach out for anything. Additionally, we have a wide service portfolio via our experienced, proficient and skilled talent pool. If you are dissatisfied with our services, we will refund your money back immediately, no question asked.
</p>
<p style="margin-top: 19px; color: yellowgreen;">Still have questions in mind?  Feel free to reach us anytime.</p>
 
</div>

            <div class="col-lg-5 col-xs-5"></div>
            <div class="col-lg-7 how_txt">
               
            </div>
        </div>
    </div>

    <div class="top_wide reach_out_us" >
        <div class="container">
            <div class="row light text-center plansec">
                <div class="col-lg-12">
                    <div class="col-lg-12 "><h2><strong></strong>
                        Reach out to us
</h2></div>
                   <!--<div class="col-lg-2 col-xs-4 padtop"><a href="#"><img src="images/icon_seo.png" alt="icon_seo"><span class="icon_text"></br>SEO Plan</span></a></div>
                    --></div>
            </div>
        </div>
    </div>

    <div class="container base reach_out_us">
         
        <div class="row iconhead ">       
            

            <div class="col-lg-8">
                <p class="content iconhead_pad">
                    Our experts get a complete 360 degree understanding of the areas where we can assist you. We understand that each business is unique and hence, we recommend the best solution that suits your requirements.

                 </p>
            </div>
            <div class="col-lg-4 padtop visible-lg"><p class="pull-right padtop"><img src="img/servicc.png" alt="monitor"></p></div>
        </div>
    </div>

    <div class="top_wide get_introduce">
        <div class="container">
            <div class="row text-center light">
                <h2>Get introduced</h2>
            </div>
        </div>
    </div>

    <div class="container get_introduce">
        <div class="row base">
            <div class="col-lg-6 col-xs-6"><img src="img/get_intro.png" ></div>
            <div class="col-lg-6">
                <h3>Get introduced
</h3>
                <div class="content">
                    <p>Immediately after you sign up, we identify one of our outstanding Pelicans to support you. Speak to the virtual assistant before you decide to assign task. Also explore how our Pelicans are supported by a vast team of in-house talent pool</p>
                     </div>
            </div>
        </div>
    </div>


    <div class="top_wide sign_up">
        <div class="container">
            <div class="row text-center light">
                <h2>Sign Up
</h2>
            </div>
        </div>
    </div>

    <div class="container sign_up">
        <div class="row base">
           
            <div class="col-lg-6">
                <h3>Sign Up

</h3>
                <div class="content">
                    <p>Whether you are looking for a few hours of support or full time assistance, choose the plan that suits your requirements and your budget. Our plans are flexible and you may opt for a trial to identify the best fit.
</p>
</div>
            </div>
             <div class="col-lg-6 col-xs-6">
 <div class="col-lg-6 col-xs-6">
</div>
 <div class="col-lg-6 col-xs-6">

                <img src="img/signup.png" >
</div>
            </div>
        </div>
    </div>

    <div class="top_wide get_start">
        <div class="container">
            <div class="row text-center light">
                <h2>Get started

</h2>
            </div>
        </div>
    </div>

    <div class="container get_start">
        <div class="row base">
            <div class="col-lg-6 col-xs-6"><img src="img/get_start.png" ></div>
            <div class="col-lg-6">
                <h3>Get started


</h3>
                <div class="content">
                    <p>We will take over from here. Our skilled team will ensure that there is a personal touch to everything they do. Pelicans are adequately supported by our in-house experts and hence, our capabilities extend beyond virtual virtual-assistance-hire</p>
</div>
            </div>
        </div>
    </div>
<?php
    include 'footer.php';
?>