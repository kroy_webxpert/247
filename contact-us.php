<?php include('header.php'); ?>

<div class="top">
<div class="banner_device"><img src="images/banner_subdevice.png" alt="virtual-assistance-contact"/></div>
  <div class="banner_sub_contact">
  <div class="container">
  <div class="col-lg-12">
  <div class="bannertext ">
    
  <span><img alt="leftborder" src="images/leftborder.png"></span> <h3>Contact</h3><span><img alt="rightborder" src="images/rightborder.png"></span>
  </div>
  </div>
  
  </div>
  </div>
</div>

<?php include('top.php'); ?>

<div class="top_wide">
<div class="container">
<div class="row text-center">
<h3>Got a Question? Let one of Our  Assistants help!</h3>
</div>
</div>
</div>

<div class="clearfix"></div>

<div class="container base" id="point">

  <div class="row">
  <div class="col-lg-10 col-lg-offset-1">
    
  <div class="contact_wrapper">
        <h3>Connect With Us</h3>
        <form name="contactForm" method="post" id="contactForm" action="send_mail.php">
        <input type="hidden" name="contactForm" value="contactForm">
          <div class="form-group">
            <label>Name:</label>
            <input type="text" name="name" placeholder="Enter your name" class="form-control">
           </div>
           <div class="form-group">
             <label>Phone:</label>
            <input type="text" name="phone" placeholder="Enter your phone number" class="form-control">
            </div>
            <div class="form-group">
             <label>Email:</label>
            <input type="text" name="email" placeholder="Enter your email" class="form-control">
            </div>
            <div class="form-group">
             <label>Message:</label>
             <textarea class="form-control" name="message" rows="5" cols="5" id="message"></textarea>
            </div>
            <div class="form-group">
             <label>How did you find us?:</label>
            <input type="text" placeholder="" name="find_us" class="form-control">
            </div>
            <div class="capcha_wrap">
          
        </div>
            <input type="submit"  class="btn btn-primary" value="Send">
          
         
        </form>
      
      </div>
      
      <div class="address_wrap">
      <h3>Our Location</h3>
       <p><strong>India</strong><br>

        Pelian Highway </br>
       Virtual Assistant<br>
        
        Mumbai, india<br>
        
        <span>Tel : 9852144552 </span>

 
<br><br>
Or just email us at <a href="mailto:help@pelicalhighway.com">help@pelicalhighway.com</a></p>
      </div>
      </div>
      
      
      
      
    </div>
    
  </div>

</div>
<?php 
 if(isset($_GET['sucess']) && $_GET['sucess']==true)
 {
?>

<script type="text/javascript">
  alert('Thank You for Connect With Us !');
</script>
 
<?php } include('footer.php'); ?>