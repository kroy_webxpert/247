$(document).ready(function(e) {
$("#captchaform").validate({
		rules: {
			name:"required",
			phone:{
				required: true,
				number:true
				},
			email:{
				required: true,
				email:true
				},
			message:"required",
			find_us:"required",
			captcha: {
				required: true,
				remote: "process.php"
			}
		},
		messages: {
			name:"Enter your name",
			phone:
			{
				required:"Enter your phone number",
				number:"Enter a valid number"
			},
			email:
			{
				required: "Enter your email id",
				email:"Enter a valid email id"
			},
			message:"Enter the message",
			find_us:"Enter how you find us",
			captcha: "Invalid captcha!"
		},
		
		
		 onkeyup: false,
		
		
		
	});	
		
	

    
}); 
 

		
		// remote scripting library
		// (c) copyright 2005 modernmethod, inc
		var sajax_debug_mode = false;
		var sajax_request_type = "GET";
		var sajax_target_id = "";
		var sajax_failure_redirect = "";
		
		function sajax_debug(text) {
			if (sajax_debug_mode)
				alert(text);
		}
		
 		function sajax_init_object() {
 			sajax_debug("sajax_init_object() called..")
 			
 			var A;
 			
 			var msxmlhttp = new Array(
				'Msxml2.XMLHTTP.5.0',
				'Msxml2.XMLHTTP.4.0',
				'Msxml2.XMLHTTP.3.0',
				'Msxml2.XMLHTTP',
				'Microsoft.XMLHTTP');
			for (var i = 0; i < msxmlhttp.length; i++) {
				try {
					A = new ActiveXObject(msxmlhttp[i]);
				} catch (e) {
					A = null;
				}
			}
 			
			if(!A && typeof XMLHttpRequest != "undefined")
				A = new XMLHttpRequest();
			if (!A)
				sajax_debug("Could not create connection object.");
			return A;
		}
		
		var sajax_requests = new Array();
		
		function sajax_cancel() {
			for (var i = 0; i < sajax_requests.length; i++) 
				sajax_requests[i].abort();
		}
		
		function sajax_do_call(func_name, args) {
			var i, x, n;
			var uri;
			var post_data;
			var target_id;
			
			sajax_debug("in sajax_do_call().." + sajax_request_type + "" + sajax_target_id);
			target_id = sajax_target_id;
			if (typeof(sajax_request_type) == "undefined" || sajax_request_type == "") 
				sajax_request_type = "GET";
			
			uri = "signup.php?type=70&signup=yes&plan=BUE";
			if (sajax_request_type == "GET") {
			
				if (uri.indexOf("?") == -1) 
					uri += "?rs=" + escape(func_name);
				else
					uri += "&rs=" + escape(func_name);
				uri += "&rst=" + escape(sajax_target_id);
				uri += "&rsrnd=" + new Date().getTime();
				
				for (i = 0; i < args.length-1; i++) 
					uri += "&rsargs[]=" + escape(args[i]);

				post_data = null;
			} 
			else if (sajax_request_type == "POST") {
				post_data = "rs=" + escape(func_name);
				post_data += "&rst=" + escape(sajax_target_id);
				post_data += "&rsrnd=" + new Date().getTime();
				
				for (i = 0; i < args.length-1; i++) 
					post_data = post_data + "&rsargs[]=" + escape(args[i]);
			}
			else {
				alert("Illegal request type: " + sajax_request_type);
			}
			
			x = sajax_init_object();
			if (x == null) {
				if (sajax_failure_redirect != "") {
					location.href = sajax_failure_redirect;
					return false;
				} else {
					sajax_debug("NULL sajax object for user agent:\n" + navigator.userAgent);
					return false;
				}
			} else {
				x.open(sajax_request_type, uri, true);
				// window.open(uri);
				
				sajax_requests[sajax_requests.length] = x;
				
				if (sajax_request_type == "POST") {
					x.setRequestHeader("Method", "POST " + uri + " HTTP/1.1");
					x.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
				}
			
				x.onreadystatechange = function() {
					if (x.readyState != 4) 
						return;

					sajax_debug("received " + x.responseText);
				
					var status;
					var data;
					var txt = x.responseText.replace(/^\s*|\s*$/g,"");
					status = txt.charAt(0);
					data = txt.substring(2);

					if (status == "") {
						// let's just assume this is a pre-response bailout and let it slide for now
					} else if (status == "-") 
						alert("Error: " + data);
					else {
						if (target_id != "") 
							document.getElementById(target_id).innerHTML = eval(data);
						else {
							try {
								var callback;
								var extra_data = false;
								if (typeof args[args.length-1] == "object") {
									callback = args[args.length-1].callback;
									extra_data = args[args.length-1].extra_data;
								} else {
									callback = args[args.length-1];
								}
								callback(eval(data), extra_data);
							} catch (e) {
								sajax_debug("Caught error " + e + ": Could not eval " + data );
							}
						}
					}
				}
			}
			
			sajax_debug(func_name + " uri = " + uri + "post = " + post_data);
			x.send(post_data);
			sajax_debug(func_name + " waiting..");
			delete x;
			return true;
		}
		
				
		// wrapper for user_name		
		function x_user_name() {
			sajax_do_call("user_name",
				x_user_name.arguments);
		}
		
				
		// wrapper for amount		
		function x_amount() {
			sajax_do_call("amount",
				x_amount.arguments);
		}
		
				
		// wrapper for myfn		
		function x_myfn() {
			sajax_do_call("myfn",
				x_myfn.arguments);
		}
		
			
function do_multiply_cb(z) {
   if(z=='yes'){
		document.getElementById("ada").innerHTML='Already registered With this email.  Use another email for signup';
   }
	else {
	document.getElementById("ada").innerHTML='';
	}
}

function do_check(x) {
		x_user_name(x,do_multiply_cb);
	}
	
/*function do_amount_cb(z) {
	  	// document.getElementById("am").innerHTML=z;     ** Commented on Nov3 
 	  	document.getElementById("plval").value=z;
		document.getElementById("noofast").selectedIndex=0;
		
		if(z=='299'){
			document.getElementById("hrs").innerHTML=5;
			document.getElementById("pls").innerHTML='Assistant Super Lite  for Business plan';
		}
		else if(z=='499'){
			document.getElementById("hrs").innerHTML=5;
			document.getElementById("pls").innerHTML='Assistant Lite  for Business plan';
		}
		else if(z=='699'){
			document.getElementById("hrs").innerHTML=10;
			document.getElementById("pls").innerHTML='Model Assistant  for Business plan';
		}
		else if(z=='899'){
			document.getElementById("hrs").innerHTML=10;
			document.getElementById("pls").innerHTML='Super Assistant  for Business plan';
		}
	
}*/
	
function do_amount(x) {
		x_amount(x,do_amount_cb);
	}


function do_myfn(z){
// document.getElementById("am").innerHTML=z;     ** Commented on Nov3 

}

function do_myfn_x(cnt){
		var val=document.getElementById("plan").value;
		x_myfn(val,cnt,do_myfn)
	}
	
function vis(v){
	if(v=='other')
		document.getElementById('stateshow').style.display='';
	else
		document.getElementById('stateshow').style.display='none';
}

function gotohrsignup(v){
	if(v==7){
		window.location='bythehoursignup';
	}
}
function getState(val) {
	$.ajax({
	type: "POST",
	url: "get_state.php",
	data:'id='+val,
	success: function(data){
		$("#state-list").html(data);
	}
	});
}

function selectCountry(val) {
$("#search-box").val(val);
$("#suggesstion-box").hide();
}


$("#signup").validate({
			rules: {
				fname: {
					required: true,
					},
				lname: {
					required: true,
				},
				addr1: {
					required: true,
					},
				city: {
					required: true,
					},
				state: {
					required: true,
					},
				zip: {
					required: true,
					},
				country: {
					required: true,
					},
				email: {
					required: true,
					email: true,
					},
				pass: {
					required: true,
					minlength: 5,
    				maxlength: 10
					},
				confirmpass: {
					required: true,
					equalTo: "#pass",
    				minlength: 5,
    				maxlength: 10
					},
				hono: {
					required: true,
					},
				hearaboutus: {
					required: true,
					},
				creditCardNumber: {
					required: true,
					},
				creditCardType: {
					required: true,
					},
				expDateMonth: {
					required: true,
					},
				expDateYear: {
					required: true,
					},
				cvv2Number: {
					required: true,
					},
			captcha: {
				required: true,
				remote: "process.php"
			},
				agree1: {
					required: true,
					},
					
			},
			messages: {
				fname: {
					required: "First name is required",
					},
				lname: {
					required: "Last name is required",
				},
				addr1: {
					required: "Address is required",
				},
				city: {
					required: "Please select the city",
				},
				state: {
					required: "Please select the state",
				},
				zip: {
					required: "Please select the zip code",
				},
				country: {
					required: "Please Select the country",
				},
				email: {
					required: "Email field is required and must be an email address",
				},
				pass: {
					required: "Password field is required",
				},
				confirmpass: {
					required: "Confirm password field is required",
				},
				hono: {
					required: "Phone number is required",
				},
				hearaboutus: {
					required: "Hear about us field is required",
				},
				creditCardType: {
					required: "Please select credit card type",
				},
				creditCardNumber: {
					required: "Credit card number is required",
				},
				expDateMonth: {
					required: "Month is required",
				},
				expDateYear: {
					required: "Year is required",
				},
				cvv2Number: {
					required: "CVV number is required",
				},

				agree: {
					required: "Usage policy is required",
				},
				agree1: {
					required: "Privacy policy is required",
				},
					captcha: "Invalid captcha!"
			},
			errorElement: "div",
		});
		
		 $(window).load(function () {
            $("#flexiselDemo1").flexisel();
            $("#flexiselDemo2").flexisel({
                enableResponsiveBreakpoints: true,
                responsiveBreakpoints: {
                    portrait: {
                        changePoint: 480,
                        visibleItems: 1
                    },
                    landscape: {
                        changePoint: 640,
                        visibleItems: 2
                    },
                    tablet: {
                        changePoint: 768,
                        visibleItems: 3
                    }
                }
            });

            $("#flexiselDemo3").flexisel({
                visibleItems: 5,
                animationSpeed: 1000,
                autoPlay: true,
                autoPlaySpeed: 3000,
                pauseOnHover: true,
                enableResponsiveBreakpoints: true,
                responsiveBreakpoints: {
                    portrait: {
                        changePoint: 480,
                        visibleItems: 1
                    },
                    landscape: {
                        changePoint: 640,
                        visibleItems: 2
                    },
                    tablet: {
                        changePoint: 768,
                        visibleItems: 3
                    }
                }
            });

            $("#flexiselDemo4").flexisel({
                clone: false
            });

        });
$(document).ready(function (e) {
            $("#captchaform").validate({
                errorClass: 'error_msgs',
                rules: {
                    name: "required",
                    phone: {
                        required: true,
                        number: true,
                        remote: "phone_validation.php"
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    message: "required",
                },
                messages: {
                    name: "Enter your name",
                    phone:
                            {
                                required: "Enter your phone number",
                                number: "Enter a valid number",
                                remote: "Eneter a valid phone number"
                            },
                    email:
                            {
                                required: "Enter your email id",
                                email: "Enter a valid email id"
                            },
                    message: "Enter the message",
                    find_us: "Enter how you find us"
                },
                onkeyup: false,
            });

        });
 $(document).ready(function (e) {
            $("#captchaform1").validate({
                errorClass: 'error_msgs',
                rules: {
                    name: "required",
                    phone: {
                        required: true,
                        number: true,
                        remote: "phone_validation.php"
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    message: "required",
                },
                messages: {
                    name: "Enter your name",
                    phone:
                            {
                                required: "Enter your phone number",
                                number: "Enter a valid number",
                                remote: "Eneter a valid phone number"
                            },
                    email:
                            {
                                required: "Enter your email id",
                                email: "Enter a valid email id"
                            },
                    message: "Enter the message",
                    find_us: "Enter how you find us"
                },
                onkeyup: false,
            });

      
$("#login").validate({
			rules: {
				username: {
					required: true,
					email: true
					},
				password: {
					required: true,
				},
			},
			messages: {
				username: {
					required: "Please enter your email id",
					},
				password: {
					required: "Please enter a password",
				},
			},
			errorElement: "div",
		});

  });