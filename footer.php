<div class="socialmediabox" style="background-color:rgb(79,89,98)">
    <div class="container">
        <div class="row text-center">
            <div class="socialmedia ">
                <h2>Connect Us Everywhere...
</h2>
                <ul class="social_icon">
                    <li><a class="fb" href="https://www.facebook.com/PelicanHighwayVAs/?fref=ts" target="_blank"></a></li>
                    <li><a class="twitter" href="https://twitter.com/PelicanHighway" target="_blank"></a></li>
                    <li><a class="gplus" href="https://plus.google.com/105377530396081456546" target="_blank"></a></li>
                     
                </ul>
            </div>
        </div>
    </div>
</div>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3">
                <h3>About Us</h3>
                <hr>
                <p style="padding-right: 14px; text-align: justify;">Pelican Highway specializes in providing cost-effective Virtual Assistants who work remotely from their base locations. We help companies and busy professionals outsmart their competitors by leveraging the skills and the outstanding productivity of our large talent base.  </p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
                <h3>Our Services</h3>
                <hr>
                <ul>
                    <li><a href="#">Work Life Harmony </a></li>
                    <li><a href="#">Stay Ahead      </a></li>
                    <li><a href="#p">Spider Management </a></li>
                    
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
                <h3>Quick Link</h3>
                <hr>
                <ul>
                     
                    <li><a href="#">Faq</a></li>
                    <li><a href="#">Blog</a></li>
                    <li><a href="#">Plans</a></li>
                     <li><a href="#">Contact Us</a></li>
                     
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
                <h3>Reach Us</h3>
                <hr>
                <ul class="our_location">

                    <span><img alt="" src="images/route.png"></span><li> 
                     pelicanhighway 
                     <br>
                     666, Mumbai, India  
                </li>

                    <span><img alt="" src="images/phone.png"></span><li>Ph:0000000000</li>

                    <li>Enquiry: abc@pelicanhighway.com </li>
                </ul>
            </div>

        </div>
    </div>
</footer>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
  <script>
   $(".form-group:nth-child(2n+1)").css("margin-right", 0);
   
   </script>
         <script type="text/javascript">
            $('#myTab a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });

            $('#moreTabs a').click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });

            (function ($) {
                // Test for making sure event are maintained
                $('.js-alert-test').click(function () {
                    alert('Button Clicked: Event was maintained');
                });
                fakewaffle.responsiveTabs(['xs', 'sm']);

                 $("#captchaform1").validate({
                errorClass: 'error_msgs',
                rules: {
                    name: "required",
                    phone: {
                        required: true,
                        number: true,
                        remote: "phone_validation.php"
                    },
                    email: {
                        required: true,
                        email: true
                    },
                    message: "required",
                },
                messages: {
                    name: "Enter your name",
                    phone:
                            {
                                required: "Enter your phone number",
                                number: "Enter a valid number",
                                remote: "Eneter a valid phone number"
                            },
                    email:
                            {
                                required: "Enter your email id",
                                email: "Enter a valid email id"
                            },
                    message: "Enter the message",
                    find_us: "Enter how you find us"
                },
                onkeyup: false,
            });
                
 
            })(jQuery);



        </script>

       
 
</body>
</html>