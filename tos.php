<!DOCTYPE html>
<html lang="en">
    <head>
        <link href="https://plus.google.com/110412304848758494023" rel="publisher">
        <meta name="google-site-verification" content="0gfTllPwir_ODw8WbpLfTlCxYKeAOlwjPM34Z0TEmmQ" />
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="author" content="GDS">
        <meta name="Copyright" content="Copyright 2015 247VirtualAssistant">
        <meta name="Content-Language" content="en-us">
        <meta name="description" content="Virtual assistant Terms And Conditions">
        <meta name="keywords" content="Virtual Assistant,Personal Assistant,virtual assistants,executive assistant,online customer service">
        <title>Virtual Assistants - Terms And Conditions</title>
                <!-- Bootstrap -->
        <link href="https://247virtualassistants.com/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
        <!--<link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css">-->

        <link href="css/styles.css" rel="stylesheet">
        <link href="https://247virtualassistants.com/css/styles_dev.css" rel="stylesheet">
        <link href="https://247virtualassistants.com/css/mediaqueries.css" rel="stylesheet">
        <link href="https://247virtualassistants.com/fonts/stylesheet.css" rel="stylesheet">
        <link href='//fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,200,600' rel='stylesheet' type='text/css'>
        <link href='//fonts.googleapis.com/css?family=Source+Sans+Pro:400,300,200,600,700,900' rel='stylesheet' type='text/css'>
        <!--affiliate-->
        <link type="text/css" rel="stylesheet" href="https://247virtualassistants.com/dhtmlgoodies_calendar.css?random=20051112" media="screen"></link>
        <script type="text/javascript" src="https://247virtualassistants.com/dhtmlgoodies_calendar1.js?random=20060118"></script>
        <script language="javascript" src="https://247virtualassistants.com/js/validates.js"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="https://247virtualassistants.com/js/bootstrap.min.js"></script>
        <script src="https://247virtualassistants.com/js/responsive-tabs.js"></script>
<!--        <script src="lib/jquery.js"></script>-->
        <script src="https://247virtualassistants.com/dist/jquery.validate.js"></script>
      <!--  <script src="https://247virtualassistants.com/captcha.js"></script>-->
                <!-- Add jQuery library -->
<script type="text/javascript" src="https://code.jquery.com/jquery-latest.min.js"></script>
<!-- Add mousewheel plugin (this is optional) -->
<script type="text/javascript" src="https://247virtualassistants.com/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>

<!-- Add fancyBox -->
<link rel="stylesheet" href="https://247virtualassistants.com/fancybox/source/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
<script type="text/javascript" src="https://247virtualassistants.com/fancybox/source/jquery.fancybox.pack.js?v=2.1.5"></script>

<!-- Optionally add helpers - button, thumbnail and/or media -->
<link rel="stylesheet" href="https://247virtualassistants.com/fancybox/source/helpers/jquery.fancybox-buttons.css?v=1.0.5" type="text/css" media="screen" />
<script type="text/javascript" src="https://247virtualassistants.com/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.5"></script>
<script type="text/javascript" src="https://247virtualassistants.com/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.6"></script>

<link rel="stylesheet" href="https://247virtualassistants.com/fancybox/source/helpers/jquery.fancybox-thumbs.css?v=1.0.7" type="text/css" media="screen" />
<script type="text/javascript" src="https://247virtualassistants.com/fancybox/source/helpers/jquery.fancybox-thumbs.js?v=1.0.7"></script>
<script src="https://www.youtube.com/player_api"></script>
<script type="text/javascript" src="https://247virtualassistants.com/js/script.js"></script>
<!--<script type="text/javascript" src="http://192.168.1.50/virtualassistant/247virtualassistant/js/script.js"></script>-->    </head>
<body> 
<div class="top">
<div class="banner_device"><img src="images/banner_subdevice.png"/></div>
  <div class="banner_sub">
  <div class="container">
  <div class="col-lg-12">
  <div class="bannertext ">
    
  <span><img src="images/leftborder.png" alt=""/></span> <h3>Terms of Services</h3><span><img src="images/rightborder.png" alt=""/></span>
  </div>
  </div>
  
  </div>
  </div>
</div>
  <div class="container-top">
  <div class="container">
    <div class="row">
                    <div class="col-lg-5 col-lg-offset-3 col-sm-6 col-xs-12">
                        <ul>
                            <li><a href="https://247virtualassistants.com/story.php"><i class="fa fa-group"></i></i> 24/7 Story</a></li>
                            <li><a href="https://247virtualassistants.com/blog"><i class="fa fa-book"></i> Read Our Blog</a></li>
                            <li><a href="https://247virtualassistants.com/login.php"><i class="fa fa-sign-in"></i></i> Login</a></li>

                        </ul>

                    </div>
                    <div class="col-lg-4 col-sm-6 col-xs-12">
                    	<span class="phone_no"><!--+1 267 632 6605-->+1  678 701 3467</span>
                    	<span class="callback"><a href="callback_form.php" id="call_back" class="fancybox fancybox.iframe">Request a callback</a></span></div>
                    
                </div>
    </div>
  </div>
<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button aria-controls="navbar" aria-expanded="false" data-target="#navbar" data-toggle="collapse" class="navbar-toggle collapsed" type="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>          </button>
          <a href="index.php" class="navbar-brand"><img src="images/logo.png" alt=""></a>        </div>
          <div class="menu_wrap">
        <div class="collapse navbar-collapse" id="navbar">
          <ul class="nav navbar-nav">
            <li><a href="index.php">Home</a></li>
            <li ><a href="how.php">How it works</a></li>
            <li ><a href="what-we-can-do.php">What We Can Do</a></li>
            <li ><a href="virtual-assistant-plans-for-business.php">Plans</a></li>
            <li ><a href="contact-virtual-assistant.php">Contact Us</a></li>
            <li class="lst_rgt"><a href="signup.php">  <button type="button" class="btn btn-primary">Sign up</button></a></li>
          </ul>
                

        </div><!--/.nav-collapse -->
        </div>
      </div>
  </nav><div class="top_wide">
<div class="container">
<div class="row text-center">
<h3>User Agreement</h3>
</div>
</div>
</div>

<div class="container tac">
<div class="row base">

<div class="col-lg-12">
<div class="content">
<p>
  247 Virtual ("247 Virtual" or "We") is an Internet enabled service dedicated to assisting you in just about every facet of your day to day activities. We offer this assistance at a low cost, enabling our customers to save both time and money.
</p></div>

<h3>THIS AGREEMENT</h3>
<div class="content">
<p>
  This Agreement is a contract between you and 247 Virtual and applies to your use of 247 Virtual Services. You must read, agree with and accept all of the terms and conditions contained in this Agreement. The User Agreement is subject to change by 247 Virtual at any time, at its sole discretion, with advance notice given to the user. The most current version of the User Agreement, which will supersede all earlier versions, can be accessed through the hyperlink at the bottom of the 247 Virtual site. You should review the User Agreement regularly, to determine if there have been changes. Continued use of your membership constitutes acceptance of the most recent version of the User Agreement. In addition to reviewing this Agreement, please read our Privacy Policy in order to better understand our commitment to maintaining your privacy, as well as our use and disclosure of your information. By agreeing to the terms of this Agreement, you are also agreeing to the terms of 247 Virtual Privacy Policy, the terms of which are incorporated herein, and agree that the terms of such policy are reasonable.</p></div>
<h3>NOTICE</h3>
<div class="content"><p>
  You agree that 247 Virtual may provide notices to you of changes to our User Agreement, Privacy Policy, or any other policy or issue 247 Virtual may have, by website postings, emails to the email address listed in your account, or by mail to the street address listed in your account. Such notices shall be considered to be received by you within 24 hours of the time they are posted to our website or sent by email to you unless we receive notice that the email was not delivered. Any notice sent by mail will be considered to have been received by you three business days after it is sent.
</p>
</div>
<h3>OWNERSHIP</h3>
<div class="content"><p>
  This site, together with the arrangement and compilation of the content, is the copyrighted property of 247 Virtual. Nothing contained on this site should be construed as granting, by implication, estoppels, or otherwise, any license or right to use any of the copyrights without the written permission of 247 Virtual. 'ask247 Virtual.com,' '247 Virtual,' and all related logos, products and services described in our website are copyrighted materials. You may not copy, imitate or use them without 247 Virtual prior written consent.
</p></div>
<h3>ELIGIBILITY AND AUTHORIZATION</h3>
<div class="content"><p>
  To be eligible for our Services, you must be at least 18 years old. When signing up for our services, you authorize 247 Virtual, directly or through third parties, to make any inquiries we consider necessary to validate your identity. This may include asking you for further information, requiring you to take steps to confirm ownership of your email address or financial instruments, and verifying your information against third party databases or through other sources. 
</p></div>
<h3> FEES FOR USERS</h3>
<div class="content"><p>
  Fees for services can be obtained by contacting 247 Virtual. We reserve the right to change our fees at any time. Changes to our fee schedule are effective after we provide you with at least fourteen (14) days' notice by posting the changes on the Site or contacting you through the email or mailing address listed in your account. Changes in fees for current customers or clients become effective at the beginning of their next billing period. We may choose to temporarily change the fees for our services for promotional events or new services, and such changes are effective when we post the temporary promotional event or new service on the Site.
</p></div>

<h3>GIFTS</h3>
<div class="content"><p>
  Gifts may be purchased to be gifted to a third party. Gift may be purchased as plan subscription or hourly plans. Once purchased the user register to use their gift within 30 days. Gift cannot be exchanged for cash value. 
</p></div>

<h3>CLOSING YOUR ACCOUNT</h3>
<div class="content"><p>
  You may close your Account at any time by logging in to the 247 Virtual portals and following the instructions to cancel membership or by contacting a 247 Virtual Representative. If your account is not closed before your next billing cycle your account will be renewed .Non active account will be automatically closed after 6 months
<p><br>

<p><b>Usage of Hours</b> All hours purchased must be used within 30 days of purchase. Clients can rollover up to 25% of the hours left in their current month to the next month of service. Rollover may not be applied to trial hours, special priced plans and noncurrent or non active plans </p>
</div>
<h3>REFUND</h3>
<div class="content">
  <p>We try to make sure that every client is satisfied as possible. However once a renewal is processed we will not be able to grant refunds not including exceptions stated in the TOS. If you plan on discontinuing your membership you can cancel your account any time before the next renewal date by emailing to <a href="mailto:assistme@247virtualassistant.com">assistme@247virtualassistant.com</a> . If you decide to reactivate your account at a later time, you can most definitely get the credit of the balance amount  transferred to your account . </p>
</div>
<h3>TERMS OF USAGE</h3>
<div class="content"><p>
  The services that 247 Virtual provides are strictly for the registered user only. We will not be held accountable for any information that is used by a third party not privy to this agreement.
</p></div>
<h3>EXCLUSION OF WARRANTY</h3>
<div class="content"><p class="caps">
  247 VIRTUAL AND ANY THIRD PARTY PROVIDERS MAKE NO WARRANTY OF ANY KIND REGARDING THIS SITE AND/OR ANY MATERIALS PROVIDED ON THIS SITE, ALL OF WHICH ARE PROVIDED ON AN 'AS IS' BASIS. 247 VIRTUAL AND ANY THIRD PARTY PROVIDERS DO NOT WARRANT THE ACCURACY, COMPLETENESS, CURRENCY OR RELIABILITY OF ANY OF THE CONTENT OR DATA FOUND ON THIS SITE AND SUCH PARTIES EXPRESSLY DISCLAIM ALL WARRANTIES AND CONDITIONS, INCLUDING IMPLIED WARRANTIES AND CONDITIONS OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT, AND THOSE ARISING BY STATUTE OR OTHERWISE IN LAW OR FROM A COURSE OF DEALING OR USAGE OF TRADE. 247 VIRTUAL WILL NOT BE HELD LIABLE FOR THE ACCURACY, COMPLETENESS, CURRENCY OR RELIABILITY OF THE CONTENT OR DATA PROVIDED TO ANY INDIVIDUAL OR FOR ANY BUSINESS, INVESTMENT, COST, OR LOSS ASSOCIATED WITH THE INFORMATION WE PROVIDED. NEITHER 247 VIRTUAL NOR ANY THIRD PARTY PROVIDERS WARRANT THAT THIS SITE, ITS SERVERS OR ANY E-MAIL SENT FROM 247 VIRTUAL ARE FREE OF VIRUSES OR OTHER HARMFUL COMPONENTS. SOME STATES DO NOT ALLOW THE DISCLAIMER OF IMPLIED WARRANTIES, SO THE FOREGOING DISCLAIMER MAY NOT APPLY TO YOU
</p></div>

<h3>LIMITATION OF LIABILITY</h3>
<div class="content"><p>
247 Virtual assumes no responsibility, and shall not be liable for, any damages to, or viruses that may infect your computer equipment or other property on account of your access to, use of, or browsing in this site or you're downloading of any materials, data, text, images, video or audio from the site. We are also not responsible for any loss attributed to our failure to provide timely reminders to our users. In no event shall 247 Virtual or any third party providers or distributors be liable for any injury, loss, claim, damage, or damages, including, but not limited to, any special, exemplary, punitive, indirect, incidental or consequential damages of any kind, whether based in contract, tort, strict liability, or otherwise, which arises out of or is in any way connected with (i) any use of this site or content found herein, or (ii) the performance or non performance by 247 Virtual or any third party providers, including, but not limited to, non performance resulting from bankruptcy, reorganization, insolvency, dissolution or liquidation even if such party has been advised of the possibility of damages to such parties or any other party.
</p></div>


<h3>GUARANTEES AND LIABILITIES</h3>
<div class="content"><p> If any project or work completed by 247VirtualAssistant.com results in loss to you, 247 Virtual will suffer no liability. In the event of an error in the completed task, it will be corrected by 247 without any further charges. 247 Virtual works to acheive a high level of quality, however due to the varied nature of the work, we offer no guarantees that work completed will satisfy the clients expectation of quality or time taken to complete. Refunds will only be offered based on the quality and time expectation set forth by 247 Virtual Management and not by the client.</p></div>

<h3>LIST OF HOLIDAYS:<p>Our office will be closed on the following days in 2015</p> </h3>
                         
      <table class="table table-bordered">
        
        <tbody>
          <tr>
            <td>January 1</td>
            <td>New Year's Day </td>
          </tr>
          <tr>
            <td>April 3</td>
            <td>Good Friday</td>
          </tr>
          <tr>
            <td>May 25</td>
            <td>Memorial Day</td>
          </tr>
          <tr>
            <td>July 4</td>
            <td>Independence Day </td>
          </tr>
          <tr>
            <td>September 7</td>
            <td>Labour Day </td>
          </tr>
          <tr>
            <td>November 26</td>
            <td>Thanksgiving Day </td>
          </tr>
          <tr>
            <td>December 25</td>
            <td>Christmas Day </td>
          </tr>
        </tbody>
      </table>

<h3>NONDISCLOSURE</h3>
<div class="content"><p>
The terms of the user agreement governs the disclosure of information by and between 247 Virtual, (the *Recipient*) and you, the new member (the *Discloser*) as of the date of this membership signup.</p><br>
<p>The parties are willing to disclose such information to each other on the condition that the recipient of the information does not disclose the same to any third party nor make use thereof in any manner except as set out below.</p><br>
<p>In consideration of such disclosure to each other, it is agreed by and between the parties hereto as follows:
</p></div>

<h3> Handling of Confidential Information: </h3>
<div class="content"><p>
The receiving party undertakes to treat as strictly confidential and not to divulge to any third party any of the information disclosed by the other and not to make use of any such information without the disclosing party's prior written consent. The obligations of confidentiality and non-disclosure will be honored even after the termination of this agreement, except as required by governmental authorities.
</p></div>

<h3>Definition of Confidential Information</h3>
<div class="content"><p>
As used herein, Confidential Information shall mean any and all technical and non-technical information provided by either party to the other, including but not limited to, trade secrets, information related to current, future, and proposed products and services of each of the parties, and including, without limitation, their respective information concerning research, experimental work, development, financial information, customer lists, employees, business and contractual relationships, sales and marketing plans.
</p></div>

<h3>Exceptions to Confidential Information</h3>
<div class="content"><p>
The above undertaking shall not apply to:<br>
a. Information which after disclosure by the disclosing party is published or becomes generally available to the public, otherwise than through any act or omission on the part of the receiving party;<br>
b. Information which the receiving party can show was in its possession at the time of disclosure and which was not acquired directly from the disclosing party;<br>
c. Information rightfully acquired from others who did not obtain it under the pledge of secrecy to the disclosing party.<br>
d. Information which at the time of disclosure is published or otherwise generally available to the public;<br>
</p></div>


<h3>Residual Knowledge </h3>
<div class="content"><p>
The terms of this Agreement shall be deemed to apply also to the employees or agents or legally associated entities of the receiving party who shall require their said employees or agents or legally associated entities to observe the foregoing obligations.
</p></div>

<h3> No Grant of Rights
</h3>
<div class="content"><p>
Neither the execution of this Agreement, nor the disclosure of any Proprietary Information hereunder, shall be construed as granting either expressly or by implication, estoppels or otherwise, any license under any invention or patent now or hereafter owned by or controlled by the parties.
</p></div>
<h3> INDEMNIFICATION</h3>
<div class="content"><p>
You agree to defend, indemnify and hold 247 Virtual, its officers, managers and employees harmless from any claim or demand (including attorneys' fees) made or incurred by any third party due to or arising out of your breach of this Agreement and/or your use of the Services.

</p></div>
<h3>DISPUTES </h3>
<div class="content"><p>
If a dispute arises between you and 247 Virtual please contact us first. Our goal is to learn about and address your concerns and, if we are unable to do so to your satisfaction, to provide you with a neutral and cost effective means of resolving the dispute quickly. Disputes between you and 247 Virtual regarding our services may be reported to customer service online through 247 Virtual help center at any time, or by calling us at.
</p></div>
<h3> ATTORNEY'S FEES
</h3>
<div class="content"><p>
If 247 Virtual takes any action to enforce this Agreement, 247 Virtual will be entitled to recover from you, and you agree to pay, all reasonable and necessary attorney's fees, costs, and any cost of arbitration, in addition to any other relief, at law or in equity, to which such parties may be entitled.
</p></div>

<h3> WAIVER</h3>
<div class="content"><p>
Our failure to act with respect to a breach by you or others does not waive our right to act with respect to subsequent or similar breaches.

</p></div>
<h3> TERMINATION</h3>
<div class="content"><p>
247 Virtual may terminate this Agreement and these terms and conditions and/or the provision of any of the services at any time for any reason, including any improper use of this site or your failure to comply with these terms and conditions. Such termination shall not affect any right to relief to which 247 Virtual may be entitled, at law or in equity. Upon termination of this Agreement and these terms and conditions, all rights granted to you will terminate and revert to 247 Virtual as applicable.
</p></div>

<h3>Non Solicitation
 </h3>
<div class="content"><p>
You shall not solicit any employees who are under contract with 247 in the United States or in our other International locations regardless of if they are dedicated to your projects or to other clients for a period of Twenty Four(24) Months after termination of your service contract with 247. You further agree that, should you be approached by a person who is or has been an employee of 247 during the period described above, you will not offer to nor employ or retain as an independent contractor or agent any such person for a period of 2 years following the termination of their employment. If you default on this clause or wish to hire an employee under contract with 247 as your own employee, a payment equivalent to 12 months of service from our Fulltime(40hours/week) plan would need to be made to 247 prior to releasing the employee.
</p></div>

<h3>ASSIGNMENT </h3>
<div class="content"><p>
You may not assign, convey, subcontract or delegate your rights, duties or obligations hereunder.

</p></div>
<h3> MODIFICATION</h3>
<div class="content"><p>
247 Virtual may at any time modify these terms and conditions and your continued use of this site will be conditioned upon the terms and conditions in force at the time of your use.
</p></div>
<h3>SEVERABILITY </h3>
<div class="content"><p>
These terms and conditions shall be deemed severable. In the event that any provision is determined to be unenforceable or invalid, such provision shall nonetheless be enforced to the fullest extent permitted by applicable law, and such determination shall not affect the validity and enforceability of any other remaining provisions.
</p></div>
<h3> ENTIRE AGREEMENT
</h3>
<div class="content"><p>
This Agreement, together with any terms and conditions incorporated herein or referred to herein constitute the entire agreement between us relating to the subject matter hereof, and supersedes any prior understandings or agreements (whether oral or written) regarding the subject matter, and may not be amended or modified except in writing or by making such amendments or modifications available on this site.
</p></div>
<!-- <h3> </h3>
<div class="content"><p>

</p></div>
 -->
</div>
</div>
</div>
</div>

<script type='text/javascript'>
window.__wtw_lucky_site_id = 34679;

	(function() {
		var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
		wa.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://cdn') + '.luckyorange.com/w.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
	  })();
	</script>

<div class="socialmediabox">
    <div class="container">
        <div class="row text-center">
            <div class="socialmedia ">
                <h2>Let's Be Friends! Connect With Us</h2>
                <ul class="social_icon">
                    <li><a class="fb" href="https://www.facebook.com/247VirtualAssistant" target="_blank"></a></li>
                    <li><a class="twitter" href="https://twitter.com/247Assistants" target="_blank"></a></li>
                    <li><a class="gplus" href="https://plus.google.com/+247virtualassistant/posts" target="_blank"></a></li>
                    <li><a class="instagram" href="https://instagram.com/247virtualassistant/" target="_blank"></a></li>
                    <li><a class="pin" href="https://www.pinterest.com/247virtualassis/" target="_blank"></a></li>
                    <li><a class="linkedin" href="https://www.linkedin.com/profile/view?id=389064043&trk=nav_responsive_tab_profile" target="_blank"></a></li>
                    <li><a class="rss" href="https://247virtualassistants.com/blog/feed"  target="_blank"></a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3"><a href="https://247virtualassistants.com"><img alt="" src="https://247virtualassistants.com/images/footerlogo.png"> </a><br>
                <p>Virtual assistant companies provide an efficient option for businesses that cannot afford teams of administrative assistants and in-house managers to outsource for skilled labor at cost effective charges. </p>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
                <h3>Our Services</h3>
                <hr>
                <ul>
                    <li><a href="https://247virtualassistants.com/virtual-assistant.php">Virtual Assistant</a></li>
                    <li><a href="https://247virtualassistants.com/story.php">24/7 Virtual Assistant Story</a></li>
                    <li><a href="https://247virtualassistants.com/real-estate-virtual-assistant.php">Real Esate Virtual Assistant</a></li>
                    <li><a href="https://247virtualassistants.com/remote-assistant.php">Remote Assistant</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
                <h3>Informations</h3>
                <hr>
                <ul>
                    <li><a href="https://247virtualassistants.com/resource.php">Resources</a></li>
                    <li><a href="https://247virtualassistants.com/portfolio.php">Portfolio</a></li>
                    <li><a href="https://247virtualassistants.com/faq.php">Faq</a></li>
                    <li><a href="https://247virtualassistants.com/testimonials.php">Testimonials</a></li>
                    <li><a href="https://247virtualassistants.com/careers.php">Career</a></li>
                </ul>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-3">
                <h3>Our Location</h3>
                <hr>
                <ul class="our_location">

                    <span><img alt="" src="https://247virtualassistants.com/images/route.png"></span><li>24/7 Virtual Assistant
                        1298 Braselton, Lawrenceville, GA 30043</li>

                    <span><img alt="" src="https://247virtualassistants.com/images/phone.png"></span><li>Tel: 678.701.3467</li>

                    <span><img alt="" src="https://247virtualassistants.com/images/fax.png"></span><li>Fax:(404)592-6523</li>
                </ul>
            </div>

        </div>
    </div>
</footer>
<script type='text/javascript'>
window.__wtw_lucky_site_id = 36691;

	(function() {
		var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
		wa.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://cdn') + '.luckyorange.com/w.js';
		var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
	  })();
	</script>
	
	
<!-- begin olark code -->
<!--<script data-cfasync="false" type='text/javascript'>/*<![CDATA[*/window.olark || (function (c) {
        var f = window, d = document, l = f.location.protocol == "https:" ? "https:" : "http:", z = c.name, r = "load";
        var nt = function () {
            f[z] = function () {
                (a.s = a.s || []).push(arguments)
            };
            var a = f[z]._ = {
            }, q = c.methods.length;
            while (q--) {
                (function (n) {
                    f[z][n] = function () {
                        f[z]("call", n, arguments)
                    }
                })(c.methods[q])
            }
            a.l = c.loader;
            a.i = nt;
            a.p = {
                0: +new Date};
            a.P = function (u) {
                a.p[u] = new Date - a.p[0]
            };
            function s() {
                a.P(r);
                f[z](r)
            }
            f.addEventListener ? f.addEventListener(r, s, false) : f.attachEvent("on" + r, s);
            var ld = function () {
                function p(hd) {
                    hd = "head";
                    return["<", hd, "></", hd, "><", i, ' onl' + 'oad="var d=', g, ";d.getElementsByTagName('head')[0].", j, "(d.", h, "('script')).", k, "='", l, "//", a.l, "'", '"', "></", i, ">"].join("")
                }
                var i = "body", m = d[i];
                if (!m) {
                    return setTimeout(ld, 100)
                }
                a.P(1);
                var j = "appendChild", h = "createElement", k = "src", n = d[h]("div"), v = n[j](d[h](z)), b = d[h]("iframe"), g = "document", e = "domain", o;
                n.style.display = "none";
                m.insertBefore(n, m.firstChild).id = z;
                b.frameBorder = "0";
                b.id = z + "-loader";
                if (/MSIE[ ]+6/.test(navigator.userAgent)) {
                    b.src = "javascript:false"
                }
                b.allowTransparency = "true";
                v[j](b);
                try {
                    b.contentWindow[g].open()
                } catch (w) {
                    c[e] = d[e];
                    o = "javascript:var d=" + g + ".open();d.domain='" + d.domain + "';";
                    b[k] = o + "void(0);"
                }
                try {
                    var t = b.contentWindow[g];
                    t.write(p());
                    t.close()
                } catch (x) {
                    b[k] = o + 'd.write("' + p().replace(/"/g, String.fromCharCode(92) + '"') + '");d.close();'
                }
                a.P(2)
            };
            ld()
        };
        nt()
    })({
        loader: "static.olark.com/jsclient/loader0.js", name: "olark", methods: ["configure", "extend", "declare", "identify"]});
    /* custom configuration goes here (www.olark.com/documentation) */
    olark.identify('7843-714-10-6093');/*]]>*/</script><noscript><a href="https://www.olark.com/site/7843-714-10-6093/contact" title="Contact us" target="_blank">Questions? Feedback?</a> powered by <a href="http://www.olark.com?welcome" title="Olark live chat software">Olark live chat software</a></noscript>-->
<!-- end olark code -->   <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>