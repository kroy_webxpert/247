 <?php include('header.php'); ?>

<div class="top">
<div class="banner_device"><img src="images/banner_subdevice.png" alt="virtual-assistance-login"/></div>
  <div class="banner_sub_login">
  <div class="container">
  <div class="col-lg-12">
  <div class="bannertext ">
    
  <span><img alt="leftborder" src="images/leftborder.png"></span> <h3>LOGIN</h3><span><img alt="rightborder" src="images/rightborder.png"></span>
  </div>
  </div>
  
  </div>
  </div>
</div>
  
  <?php include('top.php'); ?>

<div class="top_wide">
<div class="container">
<div class="row text-center">
<h3>Access Your Account Here</h3>
</div>
</div>
</div>

<div class="clearfix"></div>

<div class="container base" id="no">

  <div class="row">
    <div class="col-lg-9 col-lg-offset-1">
      <div class="login_wrapper" >
        <h3>Login</h3>
                        <form name="login" id="login" method="POST" action="#">
          <fieldset>
          <div class="form-group">
            <label>Email Address</label>
            <input type="text" name="username" value="" placeholder="" class="form-control">
           </div>
           <div class="form-group">
             <label>Password</label>
            <input type="password" name="password" value="" placeholder="" class="form-control">
            </div>
            <div class="login_password">
              <div class="checkbox">
      <label><input type="checkbox" name="remember" value="on">Remember Me</label>
    </div>
    <a href="#" class="forgot">Forgot Password</a>
  
            </div>
              <button type="submit" class="btn btn-primary">Submit Now</button>
          </fieldset>
        </form>
      
      </div>
      
      <div class="register_wrapper text-right">
          
     <a href="signup.php" <button type="submit" class="btn btn-primary">SIGNUP</button></a>
      </div>
      </div>
      
      
      
      
    </div>
    
  </div>

</div>

  <?php
include 'footer.php';
  ?>