<?php 
    include 'header.php';
?>

    <div id="top_second">
        <div class="top hidden-sm hidden-md visible-lg">
            <video preload="auto" autoplay loop muted  id="bg-vid">
                <source src="images/369523735.mp4" type="video/mp4">	
            </video>
        </div>
        <div class="mobile_bg hidden-lg">
                <img src="images/desktop_banner_new.jpg" alt="virtual-assistance-hire-us">
                    </div>
       <?php 
       include 'top.php';

       ?>
        <div class="sliderformholder hidden-md hidden-sm hidden-xs">
            <div class="col-lg-10 col-sm-8 col-xs-12"></div>
            <div class="col-lg-2 col-sm-4 col-xs-12">
                <div class="sliderform_base">
                    <span>For FREE Consultation</span>
                    
                    <form name="captchaform" id="captchaform" method="post"   action="#">
                        <div class="form-group">
                            <input type="text" id="name" class="form-control" name="name" placeholder="Name">
                            <span class="NameError error"></span>
                        </div>
                        <div class="form-group">
                            <input type="email" id="email" class="form-control" name="email" placeholder="E-mail">
                             <span class="EmailError error"></span>
                        </div>
                        <div class="form-group">
                            <input type="tel" id="phone" class="form-control" name="phone" placeholder="Phone no">
                            <span class="PhoneError error"></span>
                        </div>
                        <div class="form-group">
                            <textarea class="form-control" id="msg" rows="3" name="message" placeholder="What can we do for you?"></textarea>
                          <span class="MsgError error"></span>
                        </div>
                         <input type="submit"   class="submit" value="Send">

                    </form>
                </div>
            </div>
        </div>


        <div class="banner_cont">
            <div class="container text-center">
                <div class="row">
                    <div class="col-lg-12">
                        <h1 style="
    color: rgb(147, 131, 131);
">Life Optimized Life Optimized.</h1>
                        <h4>Your Personal Professional Friend
</h4>


                        <!--<a class="read_more" href="how.php">Learn More</a>-->
                        <!--  <a class="read_more" href="#" id="rd_mr">Learn More</a>-->
                        <a class="fancybox-media read_more" id="rd_mr" href="#">Learn More<i class="fa fa-play fa-lg" style="margin-left:8px"></i></a>
                    </div>
                </div>
            </div>
        </div>


        <div class="home_cont_wrap">
            <div class="socialmediabox">
    <div class="container">
        <div class="row text-center">
            <div class="socialmedia ">
                <h2>Why Pelican Highway? </h2>
                 
            </div>
        </div>
    </div>
</div>
            <div class="container">
                <div class="row base"> 
                     
                    <div class="col-md-4">
                        <img src="img/box1.jpg">
                        <h2>Work Life Harmony 
                            </h2>
                            <p class="homePost">
                                Yes, we understand that you are a busy individual. You need to take care of your valuable business, demanding job and personal chores. Where do you get you that little extra time you deserve?

                            </p>
                    </div>

                    <div class="col-md-4">
                         <img src="img/box2.jpg">
                        <h2>Stay Ahead  </h2>
                        <p class="homePost">The goal is to stay ahead to win the race and it needs your complete and undivided focus. So how do you tackle all the hassles of unproductive tasks which deviates you from focussing on the big picture?</p>

                    </div>

                    <div class="col-md-4">
                         <img src="img/box3.jpg">
                        <h2>Spider Management
</h2>
<p class="homePost">Pelicans are symbolic of the legendary Philosopher's Stone and hence, we call it Pelican management. We optimize resources and using our large group, dedicate our time and energy to save our client’s time and enable them to turn their businesses into gold.
</p>                    </div>

                </div>
            </div>
            
           

            <div class="plan_bg text-center">
                <div class="container">
                    <div class="row">
                        <h2>We have plans that works for everyone<br>
                            <span>Signup now and start Pelicanhighway dedicated services</span></h2>
                        <a href="plans.php" class="signup_now">SIGNUP NOW</a>
                    </div>
                </div>
            </div>
             
            

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="js/jquery-1.11.2.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/jquery.flexisel.js"></script>
     
 <?php 
    include 'footer.php';
 ?>